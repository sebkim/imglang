package cop5556sp17;

import static cop5556sp17.Scanner.Kind.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cop5556sp17.Parser.SyntaxException;
import cop5556sp17.Scanner.IllegalCharException;
import cop5556sp17.Scanner.IllegalNumberException;
import cop5556sp17.AST.*;

public class ASTTest {

	static final boolean doPrint = true;
	static void show(Object s){
		if(doPrint){System.out.println(s);}
	}
	

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testFactor0() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "abc";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(IdentExpression.class, ast.getClass());
	}

	@Test
	public void testFactor1() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "123";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(IntLitExpression.class, ast.getClass());
	}

	@Test
	public void testBinaryExpr0() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "1+abc";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(BinaryExpression.class, ast.getClass());
		BinaryExpression be = (BinaryExpression) ast;
		assertEquals(IntLitExpression.class, be.getE0().getClass());
		assertEquals(IdentExpression.class, be.getE1().getClass());
		assertEquals(PLUS, be.getOp().kind);
	}
	@Test
	public void testBinaryExpr1() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "1+abc*def";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(BinaryExpression.class, ast.getClass());
		BinaryExpression be = (BinaryExpression) ast;
		assertEquals(IntLitExpression.class, be.getE0().getClass());
		assertEquals(BinaryExpression.class, be.getE1().getClass());
		assertEquals(PLUS, be.getOp().kind);
		BinaryExpression be2 = (BinaryExpression)be.getE1();
		assertEquals(IdentExpression.class, be2.getE0().getClass());
		assertEquals(IdentExpression.class, be2.getE1().getClass());
		assertEquals(TIMES, be2.getOp().kind);		
	}
	@Test
	public void testBinaryExpr2() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "(1+abc)*def";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(BinaryExpression.class, ast.getClass());
		BinaryExpression be = (BinaryExpression) ast;
		assertEquals(BinaryExpression.class, be.getE0().getClass());
		assertEquals(IdentExpression.class, be.getE1().getClass());
		assertEquals(TIMES, be.getOp().kind);
		BinaryExpression be2 = (BinaryExpression)be.getE0();
		assertEquals(IntLitExpression.class, be2.getE0().getClass());
		assertEquals(IdentExpression.class, be2.getE1().getClass());
		assertEquals(PLUS, be2.getOp().kind);		
	}
	@Test
	public void testBinaryExpr3() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "a<b+c*d";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(BinaryExpression.class, ast.getClass());
		BinaryExpression be = (BinaryExpression) ast;
			Expression beLeft = be.getE0();
			assertEquals(IdentExpression.class, beLeft.getClass());
			Expression beRight = be.getE1();
			assertEquals(BinaryExpression.class, beRight.getClass());
				Expression beRightLeft = ((BinaryExpression)beRight).getE0();
				assertEquals(IdentExpression.class, beRightLeft.getClass());
				Expression beRightRight = ((BinaryExpression)beRight).getE1();
				assertEquals(BinaryExpression.class, beRightRight.getClass());
					Expression beRightRightLeft = ((BinaryExpression)beRightRight).getE0();
					assertEquals(IdentExpression.class, beRightRightLeft.getClass());
					Expression beRightRightRight = ((BinaryExpression)beRightRight).getE1();
					assertEquals(IdentExpression.class, beRightRightRight.getClass());
					assertEquals(TIMES,((BinaryExpression) beRightRight).getOp().kind);
				assertEquals(PLUS,((BinaryExpression) beRight).getOp().kind);
			assertEquals(LT, be.getOp().kind);	
	}
	@Test
	public void testBinaryExpr4() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "a+b<c*d";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.expression();
		assertEquals(BinaryExpression.class, ast.getClass());
		BinaryExpression be = (BinaryExpression) ast;
			Expression beLeft = be.getE0();
			assertEquals(BinaryExpression.class, beLeft.getClass());
				Expression beLeftLeft = ((BinaryExpression)beLeft).getE0();
				assertEquals(IdentExpression.class, beLeftLeft.getClass());
				Expression beLeftRight = ((BinaryExpression)beLeft).getE1();
				assertEquals(IdentExpression.class, beLeftRight.getClass());
				assertEquals(PLUS,((BinaryExpression) beLeft).getOp().kind);
			Expression beRight = be.getE1();
			assertEquals(BinaryExpression.class, beRight.getClass());
				Expression beRightLeft = ((BinaryExpression)beRight).getE0();
				assertEquals(IdentExpression.class, beRightLeft.getClass());
				Expression beRightRight = ((BinaryExpression)beRight).getE1();
				assertEquals(IdentExpression.class, beRightRight.getClass());
				assertEquals(TIMES,((BinaryExpression) beRight).getOp().kind);
			assertEquals(LT, be.getOp().kind);	
	}
	@Test
	public void testProgram() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "sebo { }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.program();
		assertEquals(Program.class, ast.getClass());
		Program p = (Program)ast;
		assertEquals("sebo", p.getName());
	}
	@Test
	public void testProgram2() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "seong url a, boolean b { }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.program();
		assertEquals(Program.class, ast.getClass());
		Program p = (Program)ast;
		assertEquals("seong", p.getName());
		ArrayList<ParamDec> pdArray = p.getParams();
		int index = 0;
		for(ParamDec pd: pdArray) {
			assertEquals(ParamDec.class, pd.getClass());
			if(index==0) {
				assertEquals("url", pd.getType().getText());
			}
			else if (index==1) {
				assertEquals("boolean", pd.getType().getText());
			}
			index++;
		}
	}
	@Test
	public void testBlock() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "seong2 { frame ohho sleep 5*2; boolean b while(true) {sleep 3;} if(false) {sleep 1;}}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.program();
		assertEquals(Program.class, ast.getClass());
		Program p = (Program)ast;
		Block b = p.getB();
		assertEquals(b.getFirstToken().getText(), "{");
		ArrayList<Dec> dArr = b.getDecs();
		ArrayList<Statement> sArr = b.getStatements();
		int index=0;
		for(Dec d : dArr) {
			assertEquals(Dec.class, d.getClass());
			if(index==0) {
				assertEquals("frame", d.getType().getText());
			}
			else if(index==1) {
				assertEquals("boolean", d.getType().getText());
			}
			index++;
		}
		index=0;
		for(Statement s: sArr) {
			if(index==0) {
				assertEquals(SleepStatement.class, s.getClass());
			}
			else if(index==1) {
				assertEquals(WhileStatement.class, s.getClass());
			}
			else if(index==2) {
				assertEquals(IfStatement.class, s.getClass());
			}
			index++;
		}
	}
	@Test
	public void testBlock2() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "seong2 { frame ohho  boolean b while(true) {integer i sleep 3;} if(false) {sleep 1; boolean b}}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.program();
		assertEquals(Program.class, ast.getClass());
		Program p = (Program)ast;
		Block b = p.getB();
		assertEquals(b.getFirstToken().getText(), "{");
		ArrayList<Dec> dArr = b.getDecs();
		ArrayList<Statement> sArr = b.getStatements();
		int index=0;
		for(Dec d : dArr) {
			assertEquals(Dec.class, d.getClass());
			if(index==0) {
				assertEquals("frame", d.getType().getText());
			}
			else if(index==1) {
				assertEquals("boolean", d.getType().getText());
			}
			index++;
		}
		index=0;
		for(Statement s: sArr) {
			if(index==0) {
				assertEquals(WhileStatement.class, s.getClass());
			}
			else if(index==1) {
				assertEquals(IfStatement.class, s.getClass());
			}
			index++;
		}
	}
	@Test
	public void testArg() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.arg();
		assertEquals(Tuple.class, ast.getClass());
		
	}
	@Test
	public void testArg2() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "(1, 2, 3*5, 3*2%2/3/sebo)";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.arg();
		assertEquals(Tuple.class, ast.getClass());
		Tuple ht = (Tuple)ast;
		List<Expression> eArr = ht.getExprList();
		int index=0;
		for(Expression e: eArr) {
			if(index==0 || index==1) {
				assertEquals(IntLitExpression.class, e.getClass());
			}
			else if(index==2 || index==3) {
				assertEquals(BinaryExpression.class, e.getClass());
			}
			index++;
		}
	}
	@Test
	public void testStatement1() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "sebo {sebo2 <- 3; sebo3 <- (1*2);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.program();
		assertEquals(Program.class, ast.getClass());
		Program p = (Program)ast;
		Block b = p.getB();
		ArrayList<Statement> sArr = b.getStatements();
		int index=0;
		for(Statement s: sArr) {
			assertEquals(AssignmentStatement.class, s.getClass());
			AssignmentStatement as = (AssignmentStatement)s;
			if(index==0) {
				assertEquals(as.getVar().getText(), "sebo2");
			}
			else if(index==1) {
				assertEquals(as.getVar().getText(), "sebo3");
			}
			index++;
		}
	}
	@Test
	public void testChain() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "sebo -> sebo2 |-> blur (1,2) ";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.chain();
		assertEquals(BinaryChain.class, ast.getClass());
		BinaryChain bc = (BinaryChain)ast;
		BinaryChain bcLeft = (BinaryChain)bc.getE0();
			IdentChain beLeftLeft = (IdentChain)bcLeft.getE0();
			assertEquals(IdentChain.class, beLeftLeft.getClass());
			assertEquals(ARROW, bcLeft.getArrow().kind);
			IdentChain beLeftRight = (IdentChain)bcLeft.getE1();
			assertEquals(IdentChain.class, beLeftRight.getClass());
		assertEquals(BARARROW, bc.getArrow().kind);
		ChainElem bcRight = bc.getE1();
		assertEquals(FilterOpChain.class, bcRight.getClass());
		assertEquals("blur", ((FilterOpChain)bcRight).getFirstToken().getText());
	}
	@Test
	public void overall() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "myprog integer sebo, boolean sebo { "
				+ "sleep (5);  integer sebo while(5&(3%2)>seboVal) {sleep(10); } if(true) {integer sebo while(true) {sleep 5;}}"
				+ "sebo->convolve->blur(sebo2*val<rel>rel2);  "
				+ "sebo<-5/2|3%2 *(seboVal * seoVal+(a+b)/c);"
				+ "sleep screenwidth; sleep screenheight;"
				+ "} ";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode ast = parser.parse();
	}
}

