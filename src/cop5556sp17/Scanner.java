package cop5556sp17;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Scanner {
	/**
	 * Kind enum
	 */
	private static final boolean outDetail = false;
	
	public static enum Kind {
		IDENT(""), INT_LIT(""), KW_INTEGER("integer"), KW_BOOLEAN("boolean"), 
		KW_IMAGE("image"), KW_URL("url"), KW_FILE("file"), KW_FRAME("frame"), 
		KW_WHILE("while"), KW_IF("if"), KW_TRUE("true"), KW_FALSE("false"), 
		SEMI(";"), COMMA(","), LPAREN("("), RPAREN(")"), LBRACE("{"), 
		RBRACE("}"), ARROW("->"), BARARROW("|->"), OR("|"), AND("&"), 
		EQUAL("=="), NOTEQUAL("!="), LT("<"), GT(">"), LE("<="), GE(">="), 
		PLUS("+"), MINUS("-"), TIMES("*"), DIV("/"), MOD("%"), NOT("!"), 
		ASSIGN("<-"), OP_BLUR("blur"), OP_GRAY("gray"), OP_CONVOLVE("convolve"), 
		KW_SCREENHEIGHT("screenheight"), KW_SCREENWIDTH("screenwidth"), 
		OP_WIDTH("width"), OP_HEIGHT("height"), KW_XLOC("xloc"), KW_YLOC("yloc"), 
		KW_HIDE("hide"), KW_SHOW("show"), KW_MOVE("move"), OP_SLEEP("sleep"), 
		KW_SCALE("scale"), EOF("eof"), COMMENT("comment");

		Kind(String text) {
			this.text = text;
		}

		final String text;

		String getText() {
			return text;
		}
	}
	public static enum State {
		START, DIV, IN_COMMENT_STAR, IN_COMMENT_STAR_STAR, 
		IN_EQUAL, EXCLAIM, DIGIT, IDENT, OR, OR_DASH,
		LT, GT, MINUS
	}
/**
 * Thrown by Scanner when an illegal character is encountered
 */
	@SuppressWarnings("serial")
	public static class IllegalCharException extends Exception {
		public IllegalCharException(String message) {
			super(message);
		}
	}
	
	/**
	 * Thrown by Scanner when an int literal is not a value that can be represented by an int.
	 */
	@SuppressWarnings("serial")
	public static class IllegalNumberException extends Exception {
	public IllegalNumberException(String message){
		super(message);
		}
	}
	

	/**
	 * Holds the line and position in the line of a token.
	 */
	static class LinePos {
		public final int line;
		public final int posInLine;
		
		public LinePos(int line, int posInLine) {
			super();
			this.line = line;
			this.posInLine = posInLine;
		}

		@Override
		public String toString() {
			return "LinePos [line=" + line + ", posInLine=" + posInLine + "]";
		}
	}

	public class Token {
		public Kind kind;
		public final int pos;  //position in input array
		public final int length;  
		public LinePos lp;

		//returns the text of this Token
		public String getText() {
			//TODO IMPLEMENT THIS
			String tokenContent = chars.substring(pos, pos+length);
			return tokenContent;
		}
		
		//returns a LinePos object representing the line and column of this Token
		LinePos getLinePos(){
			//TODO IMPLEMENT THIS
			return this.lp;
		}

		Token(Kind kind, int pos, int length) {
			this.kind = kind;
			this.pos = pos;
			this.length = length;
		}
		public boolean isKind(Kind kind) {
			if(this.kind == kind) return true;
			return false;
		}
		public boolean isKind(Kind... kinds) {
			for(Kind kind: kinds) {
				if(this.kind == kind) return true;
			}
			return false;
		}
		

		/** 
		 * Precondition:  kind = Kind.INT_LIT,  the text can be represented with a Java int.
		 * Note that the validity of the input should have been checked when the Token was created.
		 * So the exception should never be thrown.
		 * 
		 * @return  int value of this token, which should represent an INT_LIT
		 * @throws NumberFormatException
		 */
		public int intVal() throws NumberFormatException{
			//TODO IMPLEMENT THIS
			return Integer.parseInt(this.getText());
		}
		@Override
		  public int hashCode() {
		   final int prime = 31;
		   int result = 1;
		   result = prime * result + getOuterType().hashCode();
		   result = prime * result + ((kind == null) ? 0 : kind.hashCode());
		   result = prime * result + length;
		   result = prime * result + pos;
		   return result;
		  }

		  @Override
		  public boolean equals(Object obj) {
		   if (this == obj) {
		    return true;
		   }
		   if (obj == null) {
		    return false;
		   }
		   if (!(obj instanceof Token)) {
		    return false;
		   }
		   Token other = (Token) obj;
		   if (!getOuterType().equals(other.getOuterType())) {
		    return false;
		   }
		   if (kind != other.kind) {
		    return false;
		   }
		   if (length != other.length) {
		    return false;
		   }
		   if (pos != other.pos) {
		    return false;
		   }
		   return true;
		  }
		  private Scanner getOuterType() {
		   return Scanner.this;
		  }
	}

	Scanner(String chars) {
		this.chars = chars;
		tokens = new ArrayList<Token>();
		String linesep=System.lineSeparator();
		if(linesep.equals("\n")) {
			// Linux
			char ls = '\n';
			for(int i=0; i<chars.length();i++) {
				if(chars.charAt(i)==ls) nlPosQ.offer(i);
			} 
		} else {
			// Windows
			char ls = '\r';
			for(int i=0;i<chars.length();i++) {
				if(chars.charAt(i)==ls) {
					nlPosQ.offer(i+1);
					i++;
				}
			}
		}
	}
	
	/**
	 * Initializes Scanner object by traversing chars and adding tokens to tokens list.
	 * 
	 * @return this scanner
	 * @throws IllegalCharException
	 * @throws IllegalNumberException
	 */
	public Scanner scan() throws IllegalCharException, IllegalNumberException {
		//TODO IMPLEMENT THIS!!!!
		int pos = 0;
		int length = chars.length();
		State state = State.START;
		int startPos=0;
		char ch;
		// -1 indicates end of scanning.
		while (pos <= length) {
			ch = getCh(pos);
			switch(state) {
			case START: {
				startPos = pos;
				switch(ch) {
				case (char)(-1): {
					tokenAdd(Kind.EOF, pos, 0);
					pos++;
				} break;
				case '+': {
					tokenAdd(Kind.PLUS, startPos, 1);
					pos++;
				} break;
				case '-': {
					state = State.MINUS;
					pos++;
				} break;
				case '*': {
					tokenAdd(Kind.TIMES, startPos, 1);
					pos++;
				} break;
				case '%': {
					tokenAdd(Kind.MOD, startPos, 1);
					pos++;
				} break;
				case '/': {
					state = State.DIV;
					pos++;
				} break;
				case '0': {
					tokenAdd(Kind.INT_LIT, startPos, 1);
					pos++;
				} break;
				case ';': {
					tokenAdd(Kind.SEMI, startPos, 1);
					pos++;
				} break;
				case ',': {
					tokenAdd(Kind.COMMA, startPos, 1);
					pos++;
				} break;
				case '(': {
					tokenAdd(Kind.LPAREN, startPos, 1);
					pos++;
				} break;
				case ')': {
					tokenAdd(Kind.RPAREN, startPos, 1);
					pos++;
				} break;
				case '{': {
					tokenAdd(Kind.LBRACE, startPos, 1);
					pos++;
				} break;
				case '}': {
					tokenAdd(Kind.RBRACE, startPos, 1);
					pos++;
				} break;
				case '|': {
					state = State.OR;
					pos++;
				} break;
				case '&': {
					tokenAdd(Kind.AND, startPos, 1);
					pos++;
				} break;
				case '=': {
					state = State.IN_EQUAL;
					pos++;
				} break;
				case '!': {
					state = State.EXCLAIM;
					pos++;
				} break;
				case '<': {
					state = State.LT;
					pos++;
				} break;
				case '>': {
					state = State.GT;
					pos++;
				} break;
				default: {
					if(Character.isWhitespace(ch)) {
						pos++;
					}
					else if(Character.isDigit(ch)) {
						state = State.DIGIT;
						pos++;
					} else if (Character.isJavaIdentifierStart(ch)) {
						state = State.IDENT;
						pos++;
					} else {
						throw new IllegalCharException("illegal char " + ch  + " at pos " + pos);
					}
				}
				}
			} break;
			case DIGIT: {
				if(Character.isDigit(ch)) {
					pos++;
				} else {
					String tokenContent = chars.substring(startPos, pos);
					try {
						Integer.parseInt(tokenContent);
					} catch (NumberFormatException e) {
						throw new IllegalNumberException("illegal int " + tokenContent + " at pos " + startPos +  ", len " + (pos - startPos));
					}
					state = State.START;
					tokenAdd(Kind.INT_LIT, startPos, pos - startPos);	
				}
				
			} break;
			case IDENT: {
				if(Character.isJavaIdentifierPart(ch)) {
					pos++;
				} else {
					state = State.START;
					tokenAdd(Kind.IDENT, startPos, pos-startPos);
				}
			} break;
			case DIV: {
				switch(ch) {
				case '*': {
					state = State.IN_COMMENT_STAR;
					pos+=1;
				} break;
				default: {
					state = State.START;
					tokenAdd(Kind.DIV, startPos, 1);
				}
				}
			} break;
			case IN_COMMENT_STAR: {
				switch(ch) {
				case '*': {
					state = State.IN_COMMENT_STAR_STAR;
					pos+=1;
				} break;
				default: {
					pos++;
				}
				}
			} break;
			case IN_COMMENT_STAR_STAR: {
				switch(ch) {
				case '/': {
//					tokenAdd(Kind.COMMENT, startPos, pos - startPos +1);
					state = State.START;
					pos+=1;
				} break;
				case '*': {
					pos++;
				} break;
				default: {
					state = State.IN_COMMENT_STAR;
					pos++;
				}
				}
			} break;
			case IN_EQUAL: {
				switch(ch) {
				case '=': {
					tokenAdd(Kind.EQUAL, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				default: {
					throw new IllegalCharException("illegal char " + ch + " at pos " + pos + " in IN_EQUAL state");
				}
				}
			} break;
			case EXCLAIM: {
				switch(ch) {
				case '=': {
					tokenAdd(Kind.NOTEQUAL, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				default: {
					tokenAdd(Kind.NOT, startPos, pos-startPos);
					state = State.START;
				}
				}
			} break;
			case OR: {
				switch(ch) {
				case '-': {
					state = State.OR_DASH;
					pos++;
				} break;
				default: {
					tokenAdd(Kind.OR, startPos, pos-startPos);
					state = State.START;
				}
				}
			} break;
			case OR_DASH: {
				switch(ch) {
				case '>': {
					tokenAdd(Kind.BARARROW, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				default: {
					tokenAdd(Kind.OR, startPos, 1);
					tokenAdd(Kind.MINUS, startPos+1, 1);
					state = State.START;
				}
				}
			} break;
			case LT: {
				switch(ch) {
				case '=': {
					tokenAdd(Kind.LE, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				case '-': {
					tokenAdd(Kind.ASSIGN, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				}  break;
				default: {
					tokenAdd(Kind.LT, startPos, pos-startPos);
					state = State.START;
				}
				}
			} break;
			case GT: {
				switch(ch) {
				case '=': {
					tokenAdd(Kind.GE, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				default: {
					tokenAdd(Kind.GT, startPos, pos-startPos);
					state = State.START;
				}
				}
			} break;
			case MINUS: {
				switch(ch) {
				case '>': {
					tokenAdd(Kind.ARROW, startPos, pos-startPos+1);
					state = State.START;
					pos++;
				} break;
				default: {
					tokenAdd(Kind.MINUS, startPos, pos-startPos);
					state = State.START;
				}
				}
			} break;
			default: {
				;
			}
			}
		}
		// organize reserved things
		for(Token t: tokens) {
			String tokenContent = t.getText();
			String [] reservedList = {"integer", "boolean", "image", "url", "file", "frame", "while", "if", "sleep", "screenheight", "screenwidth",
					"true", "false", "gray", "convolve", "blur", "scale", "width", "height", "xloc", "yloc", "hide", "show", "move"};
			for(String each: reservedList) {
				if(tokenContent.equals(each)) {
					for(Kind kind: Kind.values()) {
						if(each == kind.getText()) {
							t.kind = kind;
						}
					}
				}
			}
		}
		int hereLSInd;
		if(nlPosQ.size()!=0) hereLSInd = nlPosQ.poll();
		else hereLSInd = Integer.MAX_VALUE;
		int hereLine = 0;
		int prevLSInd = -1;
		for(Token t: tokens) {
			if(t.pos+t.length-1 < hereLSInd) {
				;
			} else {
				hereLine+=1;
				prevLSInd = hereLSInd;
				if(nlPosQ.size()!=0) hereLSInd = nlPosQ.poll();
				else hereLSInd = Integer.MAX_VALUE;
			}
			t.lp = new LinePos(hereLine, t.pos - prevLSInd-1);
			if(outDetail) {
				String tokenContent = t.getText();
				System.out.println(String.format("%s, pos=%d, len=%d, %s", t.kind, t.pos, t.length, tokenContent));
//				if(t.kind!=Kind.EOF) System.out.println(t.intVal()); // it will throw exception when other than int_lit is inputted.
				assert t.length == tokenContent.length();
				System.out.println(t.lp.toString());
				System.out.println();
			}
		}
		if(outDetail) {
			int hereLevel = 0;
			for(Token t: tokens) {
				if(t.lp.line != hereLevel) {
					System.out.println();
					hereLevel+=1;
				}
				System.out.print(t.getText() + " ");
			}
			System.out.println("");
		}
		return this;
	}
	private char getCh(int pos) {
		return pos < chars.length() ? chars.charAt(pos): (char)-1;
	}
	private void tokenAdd(Kind kind, int startPos, int len)
	{
		tokens.add(new Token(kind, startPos, len));
	}


	final ArrayList<Token> tokens;
	final String chars;
	int tokenNum;
	Queue<Integer> nlPosQ = new LinkedList<Integer>();
	/*
	 * Return the next token in the token list and update the state so that
	 * the next call will return the Token..  
	 */
	public Token nextToken() {
		if (tokenNum >= tokens.size())
			return null;
		return tokens.get(tokenNum++);
	}
	
	/*
	 * Return the next token in the token list without updating the state.
	 * (So the following call to next will return the same token.)
	 */
	public Token peek(){
		if (tokenNum >= tokens.size())
			return null;
		return tokens.get(tokenNum);		
	}
	
	/**
	 * Returns a LinePos object containing the line and position in line of the 
	 * given token.  
	 * 
	 * Line numbers start counting at 0
	 * 
	 * @param t
	 * @return
	 */
	public LinePos getLinePos(Token t) {
		//TODO IMPLEMENT THIS
		return t.lp;
	}


}
