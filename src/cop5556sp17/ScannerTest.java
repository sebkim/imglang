package cop5556sp17;


import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;


import cop5556sp17.Scanner.IllegalCharException;
import cop5556sp17.Scanner.IllegalNumberException;
import static cop5556sp17.Scanner.Kind.*;

public class ScannerTest {
	@Rule
    public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testEmpty() throws IllegalCharException, IllegalNumberException {
		String input = "";
		Scanner scanner = new Scanner(input);
		scanner.scan();
	}
	@Test
	public void testSemiConcat() throws IllegalCharException, IllegalNumberException {
		//input string
		String input = ";;;";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		Scanner.Token token = scanner.nextToken();
		assertEquals(SEMI, token.kind);
		assertEquals(0, token.pos);
		String text = SEMI.getText();
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		//get the next token and check its kind, position, and contents
		Scanner.Token token1 = scanner.nextToken();
		assertEquals(SEMI, token1.kind);
		assertEquals(1, token1.pos);
		assertEquals(text.length(), token1.length);
		assertEquals(text, token1.getText());
		Scanner.Token token2 = scanner.nextToken();
		assertEquals(SEMI, token2.kind);
		assertEquals(2, token2.pos);
		assertEquals(text.length(), token2.length);
		assertEquals(text, token2.getText());
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);
	}
	
	
	/**
	 * This test illustrates how to check that the Scanner detects errors properly. 
	 * In this test, the input contains an int literal with a value that exceeds the range of an int.
	 * The scanner should detect this and throw and IllegalNumberException.
	 * 
	 * @throws IllegalCharException
	 * @throws IllegalNumberException
	 */
	@Test
	public void testIntOverflowError() throws IllegalCharException, IllegalNumberException{
		String input = "99999999999999999";
		Scanner scanner = new Scanner(input);
		thrown.expect(IllegalNumberException.class);
		scanner.scan();		
	}
	
	@Ignore
	@Test
	public void testScan() throws IllegalCharException, IllegalNumberException{
		String input = "+-";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		
	}

//TODO  more tests
	@Ignore
	@Test
	public void testComment() throws IllegalCharException, IllegalNumberException {
		String input = "/* it is a comment */";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		Scanner.Token token = scanner.nextToken();
		assertEquals(COMMENT, token.kind);
		assertEquals(0, token.pos);
		String text = input;
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);
		
		input = " /* it is a comment */";
		//create and initialize the scanner
		scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		token = scanner.nextToken();
		assertEquals(COMMENT, token.kind);
		assertEquals(1, token.pos);
		text = input;
		assertEquals(text.length(), token.length+1);
		assertEquals(text, " " + token.getText());
		//check that the scanner has inserted an EOF token at the end
		token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);
	}
	
	@Test
	public void test1() throws IllegalCharException, IllegalNumberException {
		String input = "03 + 23 / 320 ; $_A * _$B - 100 ";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		Scanner.Token token = scanner.nextToken();
		assertEquals(INT_LIT, token.kind);
		assertEquals(0, token.pos);
		String text = "0";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(INT_LIT, token.kind);
		text = "3";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(PLUS, token.kind);
		text = "+";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(INT_LIT, token.kind);
		text = "23";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText()); 
		
		token = scanner.nextToken();
		assertEquals(DIV, token.kind);
		text = "/";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(INT_LIT, token.kind);
		text = "320";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(SEMI, token.kind);
		text = ";";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(IDENT, token.kind);
		text = "$_A";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(TIMES, token.kind);
		text = "*";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(IDENT, token.kind);
		text = "_$B";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(MINUS, token.kind);
		text = "-";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(INT_LIT, token.kind);
		text = "100";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);	
	}
	
	@Test
	public void test_operator() throws IllegalCharException, IllegalNumberException {
		String input = "|  & == != < > <= >= % !! -> |-> <-";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		Scanner.Token token = scanner.nextToken();
		assertEquals(OR, token.kind);
		assertEquals(0, token.pos);
		String text = "|";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(AND, token.kind);
		text = "&";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(EQUAL, token.kind);
		text = "==";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(NOTEQUAL, token.kind);
		text = "!=";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText()); 
		
		token = scanner.nextToken();
		assertEquals(LT, token.kind);
		text = "<";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(GT, token.kind);
		text = ">";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(LE, token.kind);
		text = "<=";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(GE, token.kind);
		text = ">=";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(MOD, token.kind);
		text = "%";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(NOT, token.kind);
		text = "!";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(NOT, token.kind);
		text = "!";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(ARROW, token.kind);
		text = "->";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(BARARROW, token.kind);
		text = "|->";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(ASSIGN, token.kind);
		text = "<-";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);	
	}
	
	@Test
	public void test_EQ_ERR() throws IllegalCharException, IllegalNumberException {
		String input = "=Z";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		thrown.expect(IllegalCharException.class);
		scanner.scan();	
	}
	@Test
	public void test_BARARROW() throws IllegalCharException, IllegalNumberException {
		String input = "|-|->";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		
		Scanner.Token token = scanner.nextToken();
		assertEquals(OR, token.kind);
		assertEquals(0, token.pos);
		String text = "|";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(MINUS, token.kind);
		assertEquals(1, token.pos);
		text = "-";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		
		token = scanner.nextToken();
		assertEquals(BARARROW, token.kind);
		assertEquals(2, token.pos);
		text = "|->";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);
		
	}
	@Test
	public void test_START_ERR() throws IllegalCharException, IllegalNumberException {
		String input = "abc @ ";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		thrown.expect(IllegalCharException.class);
		scanner.scan();	
	}
	
	@Test
	public void test_pos() throws IllegalCharException, IllegalNumberException {
		String input = " integer myint;\n"
				+ "boolean mybool;\n"
				+ "  convolve  ";
		//create and initialize the scanner
		Scanner scanner = new Scanner(input);
		scanner.scan();
		//get the first token and check its kind, position, and contents
		Scanner.Token token = scanner.nextToken();
		assertEquals(KW_INTEGER, token.kind);
		assertEquals(1, token.lp.posInLine);
		assertEquals(1, token.pos);
		String text = "integer";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(0, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(IDENT, token.kind);
		assertEquals(9, token.lp.posInLine);
		assertEquals(9, token.pos);
		text = "myint";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(0, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(SEMI, token.kind);
		assertEquals(14, token.pos);
		assertEquals(14, token.lp.posInLine);
		text = ";";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(0, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(KW_BOOLEAN, token.kind);
		assertEquals(0, token.lp.posInLine);
		text = "boolean";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(1, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(IDENT, token.kind);
		assertEquals(8, token.lp.posInLine);
		text = "mybool";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(1, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(SEMI, token.kind);
		assertEquals(14, token.lp.posInLine);
		text = ";";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(1, token.lp.line);
		
		token = scanner.nextToken();
		assertEquals(OP_CONVOLVE, token.kind);
		assertEquals(2, token.lp.posInLine);
		text = "convolve";
		assertEquals(text.length(), token.length);
		assertEquals(text, token.getText());
		assertEquals(2, token.lp.line);
				
		//check that the scanner has inserted an EOF token at the end
		Scanner.Token token3 = scanner.nextToken();
		assertEquals(Scanner.Kind.EOF,token3.kind);
		assertEquals(12, token3.lp.posInLine);
		assertEquals(2, token3.lp.line);
	}
	
}
