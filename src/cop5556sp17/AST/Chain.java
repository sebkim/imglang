package cop5556sp17.AST;

import cop5556sp17.Scanner.Token;
import cop5556sp17.AST.Type.TypeName;

public abstract class Chain extends Statement {
	TypeName hTypeName;
	public Chain(Token firstToken) {
		super(firstToken);
	}
	public TypeName getTypeName() {
		return this.hTypeName;
	}
	public void setTypeName(TypeName tn) {
		this.hTypeName = tn;
	}
}
