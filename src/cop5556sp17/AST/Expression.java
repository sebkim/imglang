package cop5556sp17.AST;

import cop5556sp17.Scanner.Token;
import cop5556sp17.AST.Type.TypeName;

public abstract class Expression extends ASTNode {
	
	TypeName hTypeName;
	
	protected Expression(Token firstToken) {
		super(firstToken);
	}
	public TypeName getTypeName() {
		return this.hTypeName;
	}
	public void setTypeName(TypeName tn) {
		this.hTypeName = tn;
	}
	public TypeName getType() {
		return this.getTypeName();
	}
	

	@Override
	abstract public Object visit(ASTVisitor v, Object arg) throws Exception;

}
