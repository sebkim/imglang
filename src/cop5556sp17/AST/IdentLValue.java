package cop5556sp17.AST;

import cop5556sp17.Scanner.Token;

public class IdentLValue extends ASTNode {
	Dec hDec;
	
	public IdentLValue(Token firstToken) {
		super(firstToken);
	}
	public Dec getDec() {
		return this.hDec;
	}
	public void setDec(Dec d) {
		this.hDec = d;
	}
	
	@Override
	public String toString() {
		return "IdentLValue [firstToken=" + firstToken.getText() + "]";
	}

	@Override
	public Object visit(ASTVisitor v, Object arg) throws Exception {
		return v.visitIdentLValue(this,arg);
	}

	public String getText() {
		return firstToken.getText();
	}

}
