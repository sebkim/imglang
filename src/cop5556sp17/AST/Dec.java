package cop5556sp17.AST;

import cop5556sp17.Scanner.Token;
import cop5556sp17.AST.Type.TypeName;
import cop5556sp17.Parser.SyntaxException;

public class Dec extends ASTNode {
	
	final Token ident;
	TypeName hTypeName;
	// if slot is -1, it will be paramDec.
	int slot=-1;
	public Dec(Token firstToken, Token ident) throws SyntaxException {
		super(firstToken);
		this.hTypeName = Type.getTypeName(firstToken);
		this.ident = ident;
	}
	public TypeName getTypeName() {
		return this.hTypeName;
	}
	public void setTypeName(TypeName tn) {
		this.hTypeName = tn;
	}
	
	public Token getType() {
		return firstToken;
	}

	public Token getIdent() {
		return ident;
	}
	public void setSlot(int i) {
		this.slot = i;
	}
	public int getSlot() {
		return this.slot;
	}
	
//	@Override
//	public String toString() {
//		return "Dec [ident=" + ident + ", firstToken=" + firstToken + "]";
//	}
	@Override
	public String toString() {
		return "Dec [ident=" + ident.getText() + ", firstToken=" + firstToken.getText() + "]" + ", typeName=" + this.getTypeName();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ident == null) ? 0 : ident.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Dec)) {
			return false;
		}
		Dec other = (Dec) obj;
		if (ident == null) {
			if (other.ident != null) {
				return false;
			}
		} else if (!ident.equals(other.ident)) {
			return false;
		}
		return true;
	}

	@Override
	public Object visit(ASTVisitor v, Object arg) throws Exception {
		return v.visitDec(this,arg);
	}

}
