package cop5556sp17.AST;

import cop5556sp17.Scanner.Token;

public class IdentChain extends ChainElem {
	
	Dec hDec;
	
	public Dec getDec() {
		return this.hDec;
	}
	public void setDec(Dec d) {
		this.hDec = d;
	}

	public IdentChain(Token firstToken) {
		super(firstToken);
	}


//	@Override
//	public String toString() {
//		return "IdentChain [firstToken=" + firstToken + "]";
//	}
	@Override
	public String toString() {
		return "IdentChain [firstToken=" + firstToken.getText() + "]" + ", typeName=" + this.getTypeName();
	}


	@Override
	public Object visit(ASTVisitor v, Object arg) throws Exception {
		return v.visitIdentChain(this, arg);
	}

}
