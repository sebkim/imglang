package cop5556sp17;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;

import cop5556sp17.Parser.SyntaxException;
import cop5556sp17.Scanner.IllegalCharException;
import cop5556sp17.Scanner.IllegalNumberException;


public class ParserTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testFactor0() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "abc";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		parser.factor(); parser.matchEOF();
	}

	@Test
	public void testArg() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "  (3,5) ";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		System.out.println(scanner);
		Parser parser = new Parser(scanner);
        parser.arg(); parser.matchEOF();
	}

	@Test
	public void testArgerror() throws IllegalCharException, IllegalNumberException, SyntaxException {
		String input = "  (3,) ";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		thrown.expect(Parser.SyntaxException.class);
		parser.arg();
	}

	@Test
	public void testProgram0() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 {}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
	@Test
	public void testProgramWithParamDec() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 integer sebo, boolean sebo2, file sebo3, url sebo4{}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		
	}
	@Test
	public void test_error1_ProgramWithParamDec() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 integer sebo, boolean sebo2, file sebo3, url sebo4, {}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error2_ProgramWithParamDec() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 integer sebo, boolean sebo2, file sebo3, , url sebo4 {}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error3_ProgramWithParamDec() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 , integer sebo, boolean sebo2, file sebo3, url sebo4 {}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error4() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  integer sebo, boolean sebo2, file sebo3, url sebo4 {sleep 5}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error5() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  {integer integer 5}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error6() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  {integer boolean 5}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error7() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {sleep sleep 5;  }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error8() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {while(sleep 5) {}}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error9() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {while(integer sebo5) {}}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error10() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {while(5+2(a*b-c/2)) { }}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error11() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {while(true) { int name}}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error12() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0  url sebo {while(true) { boolean name;}}";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error13() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { url sebo }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error14() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { if(myname==sebo) {sleep screenwidth*4/1;;} }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error15() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 {integer a";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_error16() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 wow";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.parse();
	}
	@Test
	public void test_consecutive_ident_in_program() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 file oh, integer oh2{}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		input = "file oh";
		parser = new Parser(new Scanner(input).scan());
		parser.paramDec(); parser.matchEOF();
	}
	@Test
	public void test_dec_in_main_block() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 {integer sebo integer sebo2 boolean sebo3 boolean sebo5 image sebo6 frame sebo7}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		
		input = "{integer sebo frame sebo2}";
		parser = new Parser(new Scanner(input).scan());
		parser.block(); parser.matchEOF();
		
		input = "integer sebo";
		parser = new Parser(new Scanner(input).scan());
		parser.dec(); parser.matchEOF();
	}
	@Test
	public void test_dec_in_mainWithParamDec_block() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 integer oh, boolean oh2, file oh3, url oh4{integer sebo integer sebo2 boolean sebo3 boolean sebo5 image sebo6 frame sebo7}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
	@Test
	public void test_decorstat_in_mainWithParamDec_block() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 integer oh, boolean oh2, file oh3,url oh4 { integer sebo integer sebo2 "
				+ "boolean sebo3 boolean sebo5 image sebo6 frame sebo7 "
				+ " sleep 5; sleep sebo; sleep screenwidth; sleep screenheight; sleep true; sleep false; "
				+ " sleep sebo; sleep (5); sleep (screenheight);}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		input = "sleep (screenheight);";
		parser = new Parser(new Scanner(input).scan());
		parser.statement(); parser.matchEOF();
	}
	@Test
	public void test_relOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { sleep 4<5; sleep (4<5); sleep 4<5>2==5!=3<=3>=1; }";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		input = "{ sleep 4<5; sleep (4<5); sleep 4<5>2==5!=3<=3>=1; }";
		parser = new Parser(new Scanner(input).scan());
		parser.block(); parser.matchEOF();
		input = "{}";
		parser = new Parser(new Scanner(input).scan());
		parser.block(); parser.matchEOF();
		input = "<";
		parser = new Parser(new Scanner(input).scan());
		parser.relOp(); parser.matchEOF();
		
	}
	@Test
	public void test_weakOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { sleep 4+5-2|1; sleep 4+5; sleep 4-2; sleep 4|1;}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
	@Test
	public void test_strongOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { sleep 5*2; sleep 5/2; sleep 5%2; sleep 4&2;}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
	@Test
	public void test_relWeakStrongOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog0 { sleep 5*2 + 4*true - false*sebo | screenwidth*sebo2 <= "
				+ "screenheight != true*false == sebo*2+4;}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
	@Test
	public void test_whileStat() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog1 { while (5+2*3<32/sebo%dongil) {"
				+ "sleep (sebo*sebo2/sebo3); integer my1 boolean my2} }";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		input = "while(5+2*3<32/sebo%dongil) {}";
		parser = new Parser(new Scanner(input).scan());
		parser.whileStatement(); parser.matchEOF();
	}
	@Test
	public void test_ifStat() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "prog1 { if (5+2*3<32/sebo%dongil>=ohmy%mother&231) {"
				+ "sleep (sebo*sebo2/sebo3); integer my1 boolean my2 sleep 1; } }";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
		input = "if(5+2*3<32) {}";
		parser = new Parser(new Scanner(input).scan());
		parser.ifStatement(); parser.matchEOF();
	}
	@Test
	public void test_arrowOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "->";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.arrowOp(); parser.matchEOF();
	}
	@Test
	public void test_arrowOp2() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "|->";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.arrowOp(); parser.matchEOF();
	}
	@Test
	public void test_filterOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "blur";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.filterOp(); parser.matchEOF();
	}
	@Test
	public void test_filterOp2() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "gray";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.filterOp(); parser.matchEOF();
	}
	@Test
	public void test_filterOp3() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "convolve";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.filterOp(); parser.matchEOF();
	}
	@Test
	public void test_frameOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "show";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.frameOp(); parser.matchEOF();
	}
	@Test
	public void test_frameOp2() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "hide";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.frameOp(); parser.matchEOF();
	}
	@Test
	public void test_frameOp3() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "move";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.frameOp(); parser.matchEOF();
	}
	@Test
	public void test_frameOp4() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "xloc";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.frameOp(); parser.matchEOF();
	}
	@Test
	public void test_frameOp5() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "yloc";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.frameOp(); parser.matchEOF();
	}
	@Test
	public void test_imageOp() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "width";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.imageOp(); parser.matchEOF();
	}
	@Test
	public void test_imageOp2() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "height";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.imageOp(); parser.matchEOF();
	}
	@Test
	public void test_imageOp3() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "scale";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.imageOp(); parser.matchEOF();
	}
	@Test
	public void test_assign() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "sebo <- 3";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.assign(); parser.matchEOF();
		
		input = "sebo <- (3)";
		parser = new Parser(new Scanner(input).scan());
		parser.assign(); parser.matchEOF();

		input = "sebo <- (3*3+2<3 / sebo)";
		parser = new Parser(new Scanner(input).scan());
		parser.assign(); parser.matchEOF();
	}
	@Test
	public void test_chainElem() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "blur (3)";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.chainElem(); parser.matchEOF();
	}
	@Test
	public void test_error_chainElem() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "convolve ()";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.chainElem();
	}
	@Test
	public void test_chain() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "sebo -> sebo2 |-> sebo3";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.chain(); parser.matchEOF();
		
		input = "sebo -> convolve (4*3+2) |-> move -> blur -> gray (4*2+2, 5/2%seb) |-> show -> "
				+ "hide -> move -> xloc -> yloc -> width -> height -> scale";
		parser = new Parser(new Scanner(input).scan());
		parser.chain(); parser.matchEOF();
		
		input = "convolve (4*3+2) |-> move -> blur -> gray (4*2+2, 5/2%seb) |-> show -> "
				+ "hide -> move -> xloc -> yloc -> width -> height -> scale";
		parser = new Parser(new Scanner(input).scan());
		parser.chain(); parser.matchEOF();
	}
	@Test
	public void test_chainOrAssign() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "sebo <- 3;";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.statement(); parser.matchEOF();
		
		input = "sebo -> sebo2 |-> sebo3 -> blur (3) |-> convolve(3) |-> scale;";
		parser = new Parser(new Scanner(input).scan());
		parser.statement(); parser.matchEOF();
	}
	@Test
	public void test_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{sebo <- 3; sebo->sebo2 |-> convolve(50); sebo<- 3*2%se2/123; aa->bb->convolve(a);}";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.block(); parser.matchEOF();
	}
	@Test
	public void test_error_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{ 3<- sebo; }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.block();
	}
	@Test
	public void test_error2_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{ sebo <-- 5; }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.block();
	}
	@Test
	public void test_error3_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{ sebo <= 5; }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.block();
	}
	@Test
	public void test_error4_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{ sebo >= 5; }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.block();
	}
	@Test
	public void test_error5_chainOrAssignStats() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "{ 5 -> sebo2 }";
		Parser parser = new Parser(new Scanner(input).scan());
		thrown.expect(Parser.SyntaxException.class);
		parser.block();
	}
	@Test
	public void test_overall() throws IllegalCharException, IllegalNumberException, SyntaxException{
		String input = "myprog integer sebo, boolean sebo { "
				+ "sleep (5);  integer sebo while(5&(3%2)>seboVal) {sleep(10); } if(true) {integer sebo while(true) {sleep 5;}}"
				+ "blur(12)->convolve(100)->sebo->convolve->blur(sebo2*val<rel>rel2);  "
				+ "sebo<-5/2|3%2 *(seboVal * seoVal+(a+b)/c);"
				+ "sleep screenwidth; sleep screenheight;"
				+ "} ";
		Parser parser = new Parser(new Scanner(input).scan());
		parser.parse();
	}
}
