package cop5556sp17;

import cop5556sp17.Scanner.Kind;
import static cop5556sp17.Scanner.Kind.*;
import cop5556sp17.Scanner.Token;
import cop5556sp17.AST.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Parser {
	private static final boolean outDetail = false;
	private static final boolean warningDetail = false;
	
	public static class Pair<L,R> {
	  private final L left;
	  private final R right;

	  public Pair(L left, R right) {
	    this.left = left;
	    this.right = right;
	  }

	  public L getLeft() { return left; }
	  public R getRight() { return right; }

	  @Override
	  public int hashCode() { return left.hashCode() ^ right.hashCode(); }

	  @Override
	  public boolean equals(Object o) {
	    if (!(o instanceof Pair)) return false;
	    Pair pairo = (Pair) o;
	    return this.left.equals(pairo.getLeft()) &&
	           this.right.equals(pairo.getRight());
	  }

	}
	
	/**
	 * Exception to be thrown if a syntax error is detected in the input.
	 * You will want to provide a useful error message.
	 *
	 */
	@SuppressWarnings("serial")
	public static class SyntaxException extends Exception {
		public SyntaxException(String message) {
			super(message);
		}
	}
	
	/**
	 * Useful during development to ensure unimplemented routines are
	 * not accidentally called during development.  Delete it when 
	 * the Parser is finished.
	 *
	 */
	@SuppressWarnings("serial")	
	public static class UnimplementedFeatureException extends RuntimeException {
		public UnimplementedFeatureException() {
			super();
		}
	}

	Scanner scanner;
	Token t;
	HashMap<String, HashMap<ArrayList<String>, HashSet<String>>> predictDictEasy;
	
	public static HashMap<String, HashSet<String>> deepcopy(
		    HashMap<String, HashSet<String>> original)
	{
	    HashMap<String, HashSet<String>> copy = new HashMap<String, HashSet<String>>();
	    for (Map.Entry<String, HashSet<String>> entry : original.entrySet())
	    {
	        copy.put(entry.getKey(),
	           // Or whatever List implementation you'd like here.
	           new HashSet<String>(entry.getValue()));
	    }
	    return copy;
	}
	
	// epsFunc and firstFunc
	interface epsAndFirstFuncs {
		boolean epsFunc(ArrayList<String> list);
		HashSet<String> firstFunc(ArrayList<String> list);
	}
	
	public static String[] getTermsAsArray() {
		String[] ret = new String[Kind.values().length];
		int index=0;
		for(Kind kind: Kind.values()) {
			ret[index++] = kind.name();
		}
		return ret;
	}
	
	public static HashMap<String, HashMap<ArrayList<String>, HashSet<String>>> getPredictSets() {
		
		final ArrayList<String> nonterms = new ArrayList<>(Arrays.asList
				("program", "prog_main", "block", "paramDec", "paramDec_tail",
				"dec", "statement", "decorstat", "decorstat_tail", "expression", 
				"whileStatement", "ifStatement", "chain", "assign", "chainElem", "arrowOp",
				"chain_tail", "filterOp", "frameOp", "imageOp", "arg", "arg_tail",
				"expression_tail", "term", "relOp", "elem", "elem_tail", "factor", 
				"term_tail", "weakOp", "strongOp"));
		final ArrayList<String> terms = new ArrayList<>(Arrays.asList(getTermsAsArray()));
		final String [][][] productionsRaw = {
			{{"program"}, {"IDENT", "prog_main"}},
			{{"prog_main"}, {"block"}},
			{{"prog_main"}, {"paramDec", "paramDec_tail", "block"}},
			{{"paramDec_tail"}, {"COMMA", "paramDec", "paramDec_tail"}},
			{{"paramDec_tail"}, {"EPSILON"}},
			{{"paramDec"}, {"KW_URL", "IDENT"}},
			{{"paramDec"}, {"KW_FILE", "IDENT"}},
			{{"paramDec"}, {"KW_INTEGER", "IDENT"}},
			{{"paramDec"}, {"KW_BOOLEAN", "IDENT"}},
			{{"decorstat"}, {"dec"}},
			{{"decorstat"}, {"statement"}},
			{{"block"}, {"LBRACE", "decorstat_tail", "RBRACE"}},
			{{"decorstat_tail"}, {"decorstat", "decorstat_tail"}},
			{{"decorstat_tail"}, {"EPSILON"}},
			{{"dec"}, {"KW_INTEGER", "IDENT"}},
			{{"dec"}, {"KW_BOOLEAN", "IDENT"}},
			{{"dec"}, {"KW_IMAGE", "IDENT"}},
			{{"dec"}, {"KW_FRAME", "IDENT"}},
			{{"statement"}, {"OP_SLEEP", "expression", "SEMI"}},
			{{"statement"}, {"whileStatement"}},
			{{"statement"}, {"ifStatement"}},
			{{"statement"}, {"chain", "SEMI"}},
			{{"statement"}, {"assign", "SEMI"}},
			{{"assign"}, {"IDENT", "ASSIGN", "expression"}},
			{{"chain"}, {"chainElem", "arrowOp", "chainElem", "chain_tail"}},
			{{"chain_tail"}, {"arrowOp", "chainElem", "chain_tail"}},
			{{"chain_tail"}, {"EPSILON"}},
			{{"whileStatement"}, {"KW_WHILE", "LPAREN", "expression", "RPAREN", "block"}},
			{{"ifStatement"}, {"KW_IF", "LPAREN", "expression", "RPAREN", "block"}},
			{{"arrowOp"}, {"ARROW"}},
			{{"arrowOp"}, {"BARARROW"}},
			{{"chainElem"}, {"IDENT"}},
			{{"chainElem"}, {"filterOp", "arg"}},
			{{"chainElem"}, {"frameOp", "arg"}},
			{{"chainElem"}, {"imageOp", "arg"}},
			{{"filterOp"}, {"OP_BLUR"}},
			{{"filterOp"}, {"OP_GRAY"}},
			{{"filterOp"}, {"OP_CONVOLVE"}},
			{{"frameOp"}, {"KW_SHOW"}},
			{{"frameOp"}, {"KW_HIDE"}},
			{{"frameOp"}, {"KW_MOVE"}},
			{{"frameOp"}, {"KW_XLOC"}},
			{{"frameOp"}, {"KW_YLOC"}},
			{{"imageOp"}, {"OP_WIDTH"}},
			{{"imageOp"}, {"OP_HEIGHT"}},
			{{"imageOp"}, {"KW_SCALE"}},
			{{"arg"}, {"EPSILON"}},
			{{"arg"} , {"LPAREN", "expression", "arg_tail", "RPAREN"}},
			{{"arg_tail"}, {"COMMA", "expression", "arg_tail"}},
			{{"arg_tail"}, {"EPSILON"}},
			{{"expression"}, {"term", "expression_tail"}},
			{{"expression_tail"}, {"relOp", "term", "expression_tail"}},
			{{"expression_tail"}, {"EPSILON"}},
			{{"term"}, {"elem", "term_tail"}},
			{{"term_tail"}, {"weakOp", "elem", "term_tail"}},
			{{"term_tail"}, {"EPSILON"}},
			{{"elem"}, {"factor", "elem_tail"}},
			{{"elem_tail"}, {"strongOp", "factor","elem_tail"}},
			{{"elem_tail"}, {"EPSILON"}},
			{{"factor"}, {"IDENT"}},
			{{"factor"}, {"INT_LIT"}},
			{{"factor"}, {"KW_TRUE"}},
			{{"factor"}, {"KW_FALSE"}},
			{{"factor"}, {"KW_SCREENWIDTH"}},
			{{"factor"}, {"KW_SCREENHEIGHT"}},
			{{"factor"}, {"LPAREN", "expression", "RPAREN"}},
			{{"relOp"}, {"LT"}},
			{{"relOp"}, {"LE"}},
			{{"relOp"}, {"GT"}},
			{{"relOp"}, {"GE"}},
			{{"relOp"}, {"EQUAL"}},
			{{"relOp"}, {"NOTEQUAL"}},
			{{"weakOp"}, {"PLUS"}},
			{{"weakOp"}, {"MINUS"}},
			{{"weakOp"}, {"OR"}},
			{{"strongOp"}, {"TIMES"}},
			{{"strongOp"}, {"DIV"}},
			{{"strongOp"}, {"AND"}},
			{{"strongOp"}, {"MOD"}}
		};
		HashMap<String, ArrayList< ArrayList<String>>> productions = new HashMap<String, ArrayList<ArrayList<String>>>();
		for(int i=0;i<productionsRaw.length;i++) {
			String[][] oneProduction = productionsRaw[i];
			String lhs = oneProduction[0][0];
			if(!productions.containsKey(lhs)) {
				ArrayList< ArrayList<String>> tmpArray = new ArrayList<ArrayList<String>>();
				productions.put(lhs, tmpArray);
			}
			ArrayList<String> rhs = new ArrayList<>(Arrays.asList(oneProduction[1]));
			productions.get(lhs).add(rhs);
		}
		HashMap<String, Boolean> epsDict = new HashMap<String, Boolean>();
		HashMap<String, HashSet<String>> firstDict = new HashMap<String, HashSet<String>>();
		// EPS values and FIRST sets for all symbols.
		for(String term: terms) {
			epsDict.put(term, false);
			HashSet<String> tmpSet = new HashSet<String>();
			tmpSet.add(term);
			firstDict.put(term, tmpSet);
		}
		for(String nonterm: nonterms) {
			// setting eps
			boolean nothing = true;
			ArrayList<ArrayList<String>> oneProduction = productions.get(nonterm);
			for(ArrayList<String> arr: oneProduction) {
				if(arr.get(0).equals("EPSILON")) {
					epsDict.put(nonterm, true);
					nothing = false;
					break;
				}
			}
			if(nothing == true) {
				epsDict.put(nonterm, false);
			}
			// setting first
			HashSet<String> tmpSet = new HashSet<String>();
			firstDict.put(nonterm, tmpSet);
		}
		HashMap<String, HashSet<String>> prevFirstDict = new HashMap<String, HashSet<String>>();
		HashMap<String, Boolean> prevEpsDict = new HashMap<String, Boolean>();
		while(!prevFirstDict.equals(firstDict) || !prevEpsDict.equals(epsDict)) {
			prevEpsDict = new HashMap<String, Boolean>(epsDict);
			prevFirstDict = Parser.deepcopy(firstDict);
			for(String nonterm: productions.keySet()) {
				for(ArrayList<String> oneRHS : productions.get(nonterm)) {
					boolean nothing = true;
					if(oneRHS.get(0).equals("EPSILON")) continue;
					for(String eachInRHS : oneRHS) {
						firstDict.get(nonterm).addAll(firstDict.get(eachInRHS));
						if(!epsDict.get(eachInRHS)) {
							nothing = false;
							break;
						}
					}
					if(nothing == true) {
						epsDict.put(nonterm, true);
					}
				}
			}
		}
		// epsFunc and firstFunc
		epsAndFirstFuncs myFuncs = new epsAndFirstFuncs() {
			public boolean epsFunc(ArrayList<String> list) {
				for(String x: list) {
					if(!epsDict.get(x)) return false;
				}
				return true;
			}
			public HashSet<String> firstFunc(ArrayList<String> list) {
				HashSet<String> retSet = new HashSet<String>();
				for(String x: list) {
					retSet.addAll(firstDict.get(x));
					if(!epsDict.get(x)) return retSet;
				}
				return retSet;
			}
		};
		HashMap<String, HashSet<String>> followDict = new HashMap<String, HashSet<String>>();
		// FOLLOW sets for all symbols
		for(String nonterm: nonterms) {
			followDict.put(nonterm, new HashSet<String>());
		}
		for(String term: terms) {
			followDict.put(term, new HashSet<String>());
		}
		HashMap<String, HashSet<String>> prevFollowDict = new HashMap<String, HashSet<String>>();
		boolean firstTry = true;
		while(firstTry || !prevFollowDict.equals(followDict)) {
			firstTry = false;
			prevFollowDict = Parser.deepcopy(followDict);
			for(String nonterm: productions.keySet()) {
				for(ArrayList<String> oneRHS: productions.get(nonterm)) {
					for(int i=0;i<oneRHS.size()-1;i++) {
						followDict.put(oneRHS.get(i), myFuncs.firstFunc(
								new ArrayList<String>(oneRHS.subList(i+1, oneRHS.size()))));
						
					}
				}
			}
			for(String nonterm: productions.keySet()) {
				for(ArrayList<String> oneRHS: productions.get(nonterm)) {
					for(int i=0;i<oneRHS.size();i++) {
						if(i==oneRHS.size()-1 || myFuncs.epsFunc(
								new ArrayList<String>(oneRHS.subList(i+1, oneRHS.size())))) {
							followDict.put(oneRHS.get(i), followDict.get(nonterm));
						}
					}
				}
			}
		}
		// PREDICT sets for all productions
		HashMap<Pair<String, ArrayList<String>>, HashSet<String>> predictDict = new 
				HashMap<Pair<String, ArrayList<String>>, HashSet<String>>();
		for(String nonterm: productions.keySet()) {
			for(ArrayList<String> oneRHS: productions.get(nonterm)) {
				Pair<String, ArrayList<String>> herePair = new Pair<String, ArrayList<String>>(nonterm, oneRHS);
				if(oneRHS.get(0).equals("EPSILON")) {
					predictDict.put(herePair,  followDict.get(nonterm));
				} else {
					predictDict.put(herePair, myFuncs.firstFunc(oneRHS));
					if(myFuncs.epsFunc(oneRHS)) {
						if(predictDict.containsKey(herePair)) {
							predictDict.get(herePair).addAll(followDict.get(nonterm));
						} else {
							predictDict.put(herePair, followDict.get(nonterm));
						}
					}
				}	
			}
		}
		// for ll(1) valdiation
		HashMap<String, ArrayList<HashSet<String>>> predictDictForValidation = new HashMap<String, ArrayList<HashSet<String>>>();
		for(Pair<String, ArrayList<String>> onePair: predictDict.keySet()) {
			ArrayList<HashSet<String>> tmpArray = new ArrayList<HashSet<String>>();
			predictDictForValidation.put(onePair.left, tmpArray);
		}
		for(Pair<String, ArrayList<String>> onePair: predictDict.keySet()) {
			HashSet<String> onePredicted = predictDict.get(onePair);
			predictDictForValidation.get(onePair.left).add(onePredicted);
		}
		for(String nonterm: predictDictForValidation.keySet()) {
			ArrayList<HashSet<String>> arr = predictDictForValidation.get(nonterm);
			HashSet<String> sourceSet, targetSet;
			for(int i=0;i<arr.size();i++) {
				sourceSet = arr.get(i);
				for(int j=i+1;j<arr.size();j++) {
					HashSet<String> intersectionSet = new HashSet<String>(sourceSet);
					targetSet = arr.get(j);
					intersectionSet.retainAll(targetSet);
					if(intersectionSet.size()!=0) {
//						System.out.println(nonterm);
//						System.out.println(sourceSet);
//						System.out.println(targetSet);
//						System.out.println("");
						if(warningDetail) {
							System.out.println("Warning: It is not LL(1) grammar.");
						}
					}
				}
			}
		}
		// predictDictEasy
		HashMap<String, HashMap<ArrayList<String>, HashSet<String>>> predictDictEasy = new HashMap<String, HashMap<ArrayList<String>, HashSet<String>>>();
		for(Pair<String, ArrayList<String>> onePair: predictDict.keySet()) {
			if(!predictDictEasy.containsKey(onePair.left)) {
				predictDictEasy.put(onePair.left, new HashMap<ArrayList<String>, HashSet<String>>());
			}
			if(!predictDictEasy.get(onePair.left).containsKey(onePair.right)) {
				predictDictEasy.get(onePair.left).put(onePair.right, predictDict.get(onePair));
			}
		}
		
//		epsDict.forEach((k,v)->System.out.println(k+" "+v));
//		firstDict.forEach((k,v)->System.out.println(k+" "+v));
//		followDict.forEach((k,v)->System.out.println(k+" "+v));
//		predictDict.forEach((k,v)->System.out.println(k.left+"/"+k.right+" "+v));
//		System.out.println("");
//		predictDictForValidation.forEach((k,v)->System.out.println(k+" "+v));
//		predictDictEasy.forEach((k,v)->System.out.println(k+" "+v));
//		int i=5;
//		System.out.println(i);
		return predictDictEasy;
	}
	
	Parser(Scanner scanner) {
		this.scanner = scanner;
		t = scanner.nextToken();	
		predictDictEasy = Parser.getPredictSets();
	}

	/**
	 * parse the input using tokens from the scanner.
	 * Check for EOF (i.e. no trailing junk) when finished
	 * 
	 * @throws SyntaxException
	 */
	Program parse() throws SyntaxException {
		Program prog= program();
		matchEOF();
		return prog;
	}

	Expression expression() throws SyntaxException {
		//TODO
		Expression e0 = null;
		Expression e1 = null;
		String lhs = "expression";
		if(predictDictEasy.get(lhs).get(Arrays.asList("term", "expression_tail")).contains(t.kind.toString())) {
			e0 = term();
			String lhs2 = "expression_tail";
			while(predictDictEasy.get(lhs2).get(Arrays.asList("relOp", "term", "expression_tail")).contains(t.kind.toString())) {
				Token op = t;
				consume();
				e1 = term();
				e0 = new BinaryExpression(e0.getFirstToken(), e0, op, e1);
			}
			if(outDetail) {
				System.out.println(e0);
			}
			return e0;
		}
		else {
			throw new SyntaxException("expression() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		 
	}
	
	Expression term() throws SyntaxException {
		//TODO
		Expression e0 = null;
		Expression e1 = null;
		String lhs = "term";
		if(predictDictEasy.get(lhs).get(Arrays.asList("elem", "term_tail")).contains(t.kind.toString())) {
			e0 = elem();
			String lhs2 = "term_tail";
			while(predictDictEasy.get(lhs2).get(Arrays.asList("weakOp", "elem", "term_tail")).contains(t.kind.toString())) {
				Token op = t;
				consume();
				e1 = elem();
				e0 = new BinaryExpression(e0.getFirstToken(), e0, op, e1);
			}
			return e0;
		}
		else {
			throw new SyntaxException("term() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}

	Expression elem() throws SyntaxException {
		//TODO
		Expression e0 = null;
		Expression e1 = null;
		String lhs = "elem";
		if(predictDictEasy.get(lhs).get(Arrays.asList("factor", "elem_tail")).contains(t.kind.toString())) {
			e0 = factor();
			String lhs2 = "elem_tail";
			while(predictDictEasy.get(lhs2).get(Arrays.asList("strongOp", "factor", "elem_tail")).contains(t.kind.toString())) {
				Token op = t;
				consume();
				e1 = factor();
				e0 = new BinaryExpression(e0.getFirstToken(), e0, op, e1);
			}
			return e0;
		}
		else {
			throw new SyntaxException("elem() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}

	Expression factor() throws SyntaxException {
		Expression e = null;
		Kind kind = t.kind;
		switch (kind) {
		case IDENT: {
			e = new IdentExpression(t);
			consume();
		}
			break;
		case INT_LIT: {
			e = new IntLitExpression(t);
			consume();
		}
			break;
		case KW_TRUE:
		case KW_FALSE: {
			e = new BooleanLitExpression(t);
			consume();
		}
			break;
		case KW_SCREENWIDTH:
		case KW_SCREENHEIGHT: {
			e = new ConstantExpression(t);
			consume();
		}
			break;
		case LPAREN: {
			consume();
			e = expression();
			match(RPAREN);
		}
			break;
		default:
			//you will want to provide a more useful error message
			throw new SyntaxException("illegal factor");
		}
		return e;
	}
	Dec dec() throws SyntaxException {
		Dec d = null;
		//TODO
		String lhs = "dec";
		Token first, ident;
		if(predictDictEasy.get(lhs).get(Arrays.asList("KW_IMAGE", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_IMAGE);
			ident = t;
			match(IDENT);
			
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_FRAME", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_FRAME);
			ident = t;
			match(IDENT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_INTEGER", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_INTEGER);
			ident = t;
			match(IDENT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_BOOLEAN", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_BOOLEAN);
			ident = t;
			match(IDENT);
		}
		else {
			throw new SyntaxException("dec() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		d = new Dec(first, ident);
		return d;
	}
	Object decorstat() throws SyntaxException {
		String lhs = "decorstat";
		if(predictDictEasy.get(lhs).get(Arrays.asList("dec")).contains(t.kind.toString())) {
			Dec d = null;
			d = dec();
			return d;
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("statement")).contains(t.kind.toString())) {
			Statement s = null;
			s = statement();
			return s;
		}
		else {
			throw new SyntaxException("decorstat() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}

	Statement statement() throws SyntaxException {
		//TODO
		String lhs = "statement";
		Statement s = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("OP_SLEEP", "expression", "SEMI")).contains(t.kind.toString())) {
			Expression e = null;
			Token first;
			first = t;
			match(OP_SLEEP);
			e = expression();
			match(SEMI);
			s = new SleepStatement(first, e);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("whileStatement")).contains(t.kind.toString())) {
			s = whileStatement();
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("ifStatement")).contains(t.kind.toString())) {
			s = ifStatement();
		}
		// ident is in both chain and assign
		else if(t.kind.toString().equals("IDENT")) {
			if(scanner.peek().isKind(ARROW) || scanner.peek().isKind(BARARROW)) {
				s = chain();
				match(SEMI);
				
			}
			else if(scanner.peek().isKind(ASSIGN)) {
				s = assign();
				match(SEMI);
			}
			else {
				throw new SyntaxException("statement() error! (inside statement, chain or assign error!)\n"
						+ "At token position " + t.pos + "\n"
						+ "Token kind: "+t.kind+"\n"
						+ "Toke content: " + t.getText() + "\n");
			}
		} else {
			if(predictDictEasy.get(lhs).get(Arrays.asList("chain", "SEMI")).contains(t.kind.toString())) {
				s = chain();
				match(SEMI);
			}
			else if(predictDictEasy.get(lhs).get(Arrays.asList("assign", "SEMI")).contains(t.kind.toString())) {
				s = assign();
				match(SEMI);
			} else {
				throw new SyntaxException("statement() error!\n"
						+ "At token position " + t.pos + "\n"
						+ "Token kind: "+t.kind+"\n"
						+ "Toke content: " + t.getText() + "\n");
				
			}
		}
		if(outDetail) {
			System.out.println(s);
		}
		return s;
	}
	
	Chain chain() throws SyntaxException {
		//TODO
		String lhs = "chain";
		if(predictDictEasy.get(lhs).get(Arrays.asList("chainElem", "arrowOp", "chainElem", "chain_tail")).contains(t.kind.toString())) {
			Statement s = null;
			Token first = t;
			Chain c0 = chainElem();
			Token op = t; 
			arrowOp(); 
			ChainElem ce1 = chainElem();
			c0 = new BinaryChain(first, c0, op, ce1);
			String lhsForChainTail = "chain_tail";
			while(predictDictEasy.get(lhsForChainTail).get(Arrays.asList("arrowOp", "chainElem", "chain_tail")).contains(t.kind.toString())) {
				op = t;
				arrowOp();
				ce1 = chainElem();
				c0 = new BinaryChain(c0.getFirstToken(), c0, op, ce1);
			}
			if(outDetail) {
				System.out.println(c0);
			}
			return c0;
		}
		else {
			throw new SyntaxException("chain() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}

	ChainElem chainElem() throws SyntaxException {
		//TODO
		String lhs = "chainElem";
		ChainElem ce = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("IDENT")).contains(t.kind.toString())) {
			Token first;
			first = t;
			match(IDENT);
			ce = new IdentChain(first);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("filterOp", "arg")).contains(t.kind.toString())) {
			ce = filterOp();
			
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("frameOp", "arg")).contains(t.kind.toString())) {
			ce = frameOp();
			
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("imageOp", "arg")).contains(t.kind.toString())) {
			ce = imageOp();
			
		}
		else {
			throw new SyntaxException("chainElem() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		return ce;
	}
	
	Tuple arg() throws SyntaxException {
		//TODO
		String lhs = "arg";
		Tuple ht = null;
		Token first = null;
		ArrayList<Expression> eArr = new ArrayList<Expression>();
		if(predictDictEasy.get(lhs).get(Arrays.asList("LPAREN", "expression", "arg_tail", "RPAREN")).contains(t.kind.toString())) {
			match(LPAREN);
			eArr.add(expression());
			first = eArr.get(0).getFirstToken();
			String lhsForArgTail = "arg_tail";
			while(predictDictEasy.get(lhsForArgTail).get(Arrays.asList("COMMA", "expression", "arg_tail")).contains(t.kind.toString())) {
				consume();
				eArr.add(expression());
			}
			match(RPAREN);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("EPSILON")).contains(t.kind.toString())) {
			;
		}
		else {
			;
		}
		ht = new Tuple(first, eArr);
		if(outDetail) {
			System.out.println(ht);
		}
		return ht;
	}

	Block block() throws SyntaxException {
		//TODO
		Block b = null;
		Token first = t;
		ArrayList<Dec> dl= new ArrayList<Dec>();
		ArrayList<Statement> sl = new ArrayList<Statement>();
		match(LBRACE);
		String lhsForDecorstatTail = "decorstat_tail";
		while(predictDictEasy.get(lhsForDecorstatTail).get(Arrays.asList("decorstat", "decorstat_tail")).contains(t.kind.toString())) {
			Object o = null;
			o = decorstat();
			if(o instanceof Dec) {
				dl.add((Dec)o);
			}
			else if(o instanceof Statement) {
				sl.add((Statement)o);
			}
		}
		match(RBRACE);
		b = new Block(first, dl, sl);
		return b;
	}

	Program program() throws SyntaxException {
		//TODO
		Program p = null;
		Token progName = t;
		match(IDENT);
		Pair<ArrayList<ParamDec>, Block> hPair;
		hPair = prog_main();
		p = new Program(progName, hPair.getLeft(), hPair.getRight());
		if(outDetail) {
			System.out.println(p);
		}
		return p;
	}
	Pair<ArrayList<ParamDec>, Block> prog_main() throws SyntaxException {
		String lhs = "prog_main";
		ArrayList<ParamDec> pdArray = new ArrayList<ParamDec>();
		Block b = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("block")).contains(t.kind.toString())) {
			b = block();
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("paramDec", "paramDec_tail", "block")).contains(t.kind.toString())) {
			pdArray.add(paramDec());
			String lhsForParamDecTail = "paramDec_tail";
			while(predictDictEasy.get(lhsForParamDecTail).get(Arrays.asList("COMMA", "paramDec", "paramDec_tail")).contains(t.kind.toString())) {
				consume();
				pdArray.add(paramDec());
			}
			b = block();
		}
		else {
			throw new SyntaxException("prog_main() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		Pair<ArrayList<ParamDec>, Block> hPair = new Pair<ArrayList<ParamDec>, Block>(pdArray, b);
		return hPair;
	}

	ParamDec paramDec() throws SyntaxException {
		//TODO
		String lhs = "paramDec";
		Token first, ident;
		if(predictDictEasy.get(lhs).get(Arrays.asList("KW_URL", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_URL);
			ident = t;
			match(IDENT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_FILE", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_FILE);
			ident = t;
			match(IDENT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_INTEGER", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_INTEGER);
			ident = t;
			match(IDENT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_BOOLEAN", "IDENT")).contains(t.kind.toString())) {
			first = t;
			match(KW_BOOLEAN);
			ident = t;
			match(IDENT);
		}
		else {
			throw new SyntaxException("paramDec() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		ParamDec pd = new ParamDec(first, ident);
		return pd;
	}

	
	
	Statement whileStatement() throws SyntaxException {
		String lhs = "whileStatement";
		if(predictDictEasy.get(lhs).get(Arrays.asList("KW_WHILE", "LPAREN", "expression", "RPAREN", "block")).contains(t.kind.toString())) {
			Token first = t;
			Expression e = null;
			Block b = null;
			match(KW_WHILE);
			match(LPAREN);
			e = expression();
			match(RPAREN);
			b= block();
			Statement s = new WhileStatement(first, e, b);
			return s;
		}
		else {
			throw new SyntaxException("whileStatement() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	Statement ifStatement() throws SyntaxException {
		String lhs = "ifStatement";
		if(predictDictEasy.get(lhs).get(Arrays.asList("KW_IF", "LPAREN", "expression", "RPAREN", "block")).contains(t.kind.toString())) {
			Token first = t;
			Expression e = null;
			Block b = null;
			match(KW_IF);
			match(LPAREN);
			e = expression();
			match(RPAREN);
			b = block();
			Statement s = new IfStatement(first, e, b);
			return s;
		}
		else {
			throw new SyntaxException("ifStatement() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	AssignmentStatement assign() throws SyntaxException {
		String lhs = "assign";
		if(predictDictEasy.get(lhs).get(Arrays.asList("IDENT", "ASSIGN", "expression")).contains(t.kind.toString())) {
			AssignmentStatement as = null;
			Expression e = null;
			Token first = t;
			IdentLValue ilv = new IdentLValue(first);
			match(IDENT);
			match(ASSIGN);
			e = expression();
			as = new AssignmentStatement(first, ilv, e);
			return as;
		}
		else {
			throw new SyntaxException("assign() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	
	void arrowOp() throws SyntaxException {
		String lhs = "arrowOp";
		if(predictDictEasy.get(lhs).get(Arrays.asList("ARROW")).contains(t.kind.toString())) {
			match(ARROW);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("BARARROW")).contains(t.kind.toString())) {
			match(BARARROW);
		}
		else {
			throw new SyntaxException("arrowOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	FilterOpChain filterOp() throws SyntaxException {
		String lhs = "filterOp";
		FilterOpChain filteroc = null;
		Token first;
		Tuple ht = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("OP_BLUR")).contains(t.kind.toString())) {
			first = t;
			match(OP_BLUR);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("OP_GRAY")).contains(t.kind.toString())) {
			first = t;
			match(OP_GRAY);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("OP_CONVOLVE")).contains(t.kind.toString())) {
			first = t;
			match(OP_CONVOLVE);
		}
		else {
			throw new SyntaxException("filterOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		ht = arg();
		filteroc = new FilterOpChain(first, ht);
		return filteroc;
	}
	FrameOpChain frameOp() throws SyntaxException {
		String lhs = "frameOp";
		FrameOpChain foc = null;
		Token first;
		Tuple ht = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("KW_SHOW")).contains(t.kind.toString())) {
			first = t;
			match(KW_SHOW);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_HIDE")).contains(t.kind.toString())) {
			first = t;
			match(KW_HIDE);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_MOVE")).contains(t.kind.toString())) {
			first = t;
			match(KW_MOVE);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_XLOC")).contains(t.kind.toString())) {
			first = t;
			match(KW_XLOC);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_YLOC")).contains(t.kind.toString())) {
			first = t;
			match(KW_YLOC);
		}
		else {
			throw new SyntaxException("frameOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		ht = arg();
		foc = new FrameOpChain(first, ht);
		return foc;
	}
	ImageOpChain imageOp() throws SyntaxException {
		String lhs = "imageOp";
		ImageOpChain ioc = null;
		Token first;
		Tuple ht = null;
		if(predictDictEasy.get(lhs).get(Arrays.asList("OP_WIDTH")).contains(t.kind.toString())) {
			first = t;
			match(OP_WIDTH);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("OP_HEIGHT")).contains(t.kind.toString())) {
			first = t;
			match(OP_HEIGHT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("KW_SCALE")).contains(t.kind.toString())) {
			first = t;
			match(KW_SCALE);
		}
		else {
			throw new SyntaxException("imageOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
		ht = arg();
		ioc = new ImageOpChain(first, ht);
		return ioc;
	}
	void relOp() throws SyntaxException {
		String lhs = "relOp";
		if(predictDictEasy.get(lhs).get(Arrays.asList("LT")).contains(t.kind.toString())) {
			match(LT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("LE")).contains(t.kind.toString())) {
			match(LE);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("GT")).contains(t.kind.toString())) {
			match(GT);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("GE")).contains(t.kind.toString())) {
			match(GE);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("EQUAL")).contains(t.kind.toString())) {
			match(EQUAL);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("NOTEQUAL")).contains(t.kind.toString())) {
			match(NOTEQUAL);
		}
		else {
			throw new SyntaxException("relOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	void weakOp() throws SyntaxException {
		String lhs = "weakOp";
		if(predictDictEasy.get(lhs).get(Arrays.asList("PLUS")).contains(t.kind.toString())) {
			match(PLUS);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("MINUS")).contains(t.kind.toString())) {
			match(MINUS);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("OR")).contains(t.kind.toString())) {
			match(OR);
		}
		else {
			throw new SyntaxException("weakOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}
	void strongOp() throws SyntaxException {
		String lhs = "strongOp";
		if(predictDictEasy.get(lhs).get(Arrays.asList("TIMES")).contains(t.kind.toString())) {
			match(TIMES);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("DIV")).contains(t.kind.toString())) {
			match(DIV);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("AND")).contains(t.kind.toString())) {
			match(AND);
		}
		else if(predictDictEasy.get(lhs).get(Arrays.asList("MOD")).contains(t.kind.toString())) {
			match(MOD);
		}
		else {
			throw new SyntaxException("strongOp() error!\n"
					+ "At token position " + t.pos + "\n"
					+ "Token kind: "+t.kind+"\n"
					+ "Toke content: " + t.getText() + "\n");
		}
	}

	/**
	 * Checks whether the current token is the EOF token. If not, a
	 * SyntaxException is thrown.
	 * 
	 * @return
	 * @throws SyntaxException
	 */
	public Token matchEOF() throws SyntaxException {
		if (t.isKind(EOF)) {
			return t;
		}
		throw new SyntaxException("expected EOF");
	}

	/**
	 * Checks if the current token has the given kind. If so, the current token
	 * is consumed and returned. If not, a SyntaxException is thrown.
	 * 
	 * Precondition: kind != EOF
	 * 
	 * @param kind
	 * @return
	 * @throws SyntaxException
	 */
	private Token match(Kind kind) throws SyntaxException {
		if (t.isKind(kind)) {
			return consume();
		}
		throw new SyntaxException("saw " + t.kind + " expected " + kind);
	}

	/**
	 * Checks if the current token has one of the given kinds. If so, the
	 * current token is consumed and returned. If not, a SyntaxException is
	 * thrown.
	 * 
	 * * Precondition: for all given kinds, kind != EOF
	 * 
	 * @param kinds
	 *            list of kinds, matches any one
	 * @return
	 * @throws SyntaxException
	 */
	private Token match(Kind... kinds) throws SyntaxException {
		// TODO. Optional but handy
		for(Kind kind: kinds) {
			if(t.isKind(kind)) {
				return consume();
			}
		}
		throw new SyntaxException("saw " + t.kind + " expected " + kinds);
	}

	/**
	 * Gets the next token and returns the consumed token.
	 * 
	 * Precondition: t.kind != EOF
	 * 
	 * @return
	 * 
	 */
	private Token consume() throws SyntaxException {
		Token tmp = t;
		t = scanner.nextToken();
		return tmp;
	}

}
