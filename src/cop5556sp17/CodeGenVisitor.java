package cop5556sp17;

import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.TraceClassVisitor;

import cop5556sp17.Scanner.Kind;
import cop5556sp17.Scanner.Token;
import cop5556sp17.AST.ASTVisitor;
import cop5556sp17.AST.AssignmentStatement;
import cop5556sp17.AST.BinaryChain;
import cop5556sp17.AST.BinaryExpression;
import cop5556sp17.AST.Block;
import cop5556sp17.AST.BooleanLitExpression;
import cop5556sp17.AST.Chain;
import cop5556sp17.AST.ChainElem;
import cop5556sp17.AST.ConstantExpression;
import cop5556sp17.AST.Dec;
import cop5556sp17.AST.Expression;
import cop5556sp17.AST.FilterOpChain;
import cop5556sp17.AST.FrameOpChain;
import cop5556sp17.AST.IdentChain;
import cop5556sp17.AST.IdentExpression;
import cop5556sp17.AST.IdentLValue;
import cop5556sp17.AST.IfStatement;
import cop5556sp17.AST.ImageOpChain;
import cop5556sp17.AST.IntLitExpression;
import cop5556sp17.AST.ParamDec;
import cop5556sp17.AST.Program;
import cop5556sp17.AST.SleepStatement;
import cop5556sp17.AST.Statement;
import cop5556sp17.AST.Tuple;
import cop5556sp17.AST.Type.TypeName;
import cop5556sp17.AST.WhileStatement;

import static cop5556sp17.AST.Type.TypeName.FRAME;
import static cop5556sp17.AST.Type.TypeName.IMAGE;
import static cop5556sp17.AST.Type.TypeName.URL;
import static cop5556sp17.Scanner.Kind.*;

public class CodeGenVisitor implements ASTVisitor, Opcodes {

	/**
	 * @param DEVEL
	 *            used as parameter to genPrint and genPrintTOS
	 * @param GRADE
	 *            used as parameter to genPrint and genPrintTOS
	 * @param sourceFileName
	 *            name of source file, may be null.
	 */
	public CodeGenVisitor(boolean DEVEL, boolean GRADE, String sourceFileName) {
		super();
		this.DEVEL = DEVEL;
		this.GRADE = GRADE;
		this.sourceFileName = sourceFileName;
	}

	ClassWriter cw;
	String className;
	String classDesc;
	String sourceFileName;

	MethodVisitor mv; // visitor of method currently under construction

	int maxSlot = 0;

	/** Indicates whether genPrint and genPrintTOS should generate code. */
	final boolean DEVEL;
	final boolean GRADE;

	@Override
	public Object visitProgram(Program program, Object arg) throws Exception {
		cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		className = program.getName();
		classDesc = "L" + className + ";";
		String sourceFileName = (String) arg;
		cw.visit(52, ACC_PUBLIC + ACC_SUPER, className, null, "java/lang/Object",
				new String[] { "java/lang/Runnable" });
		cw.visitSource(sourceFileName, null);

		// generate constructor code
		// get a MethodVisitor
		mv = cw.visitMethod(ACC_PUBLIC, "<init>", "([Ljava/lang/String;)V", null, null);
		mv.visitCode();
		// Create label at start of code
		Label constructorStart = new Label();
		mv.visitLabel(constructorStart);
		// this is for convenience during development--you can see that the code
		// is doing something.
		CodeGenUtils.genPrint(DEVEL, mv, "\nentering <init>");
		// generate code to call superclass constructor
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
		// visit parameter decs to add each as field to the class
		// pass in mv so decs can add their initialization code to the
		// constructor.
		ArrayList<ParamDec> params = program.getParams();
		for (int i = 0; i < params.size(); i++) {
			ParamDec dec = params.get(i);
			// need to pass index to access args[i]
			dec.visit(this, new Integer(i));
		}
		mv.visitInsn(RETURN);
		// create label at end of code
		Label constructorEnd = new Label();
		mv.visitLabel(constructorEnd);
		// finish up by visiting local vars of constructor
		// the fourth and fifth arguments are the region of code where the local
		// variable is defined as represented by the labels we inserted.
		mv.visitLocalVariable("this", classDesc, null, constructorStart, constructorEnd, 0);
		mv.visitLocalVariable("args", "[Ljava/lang/String;", null, constructorStart, constructorEnd, 1);
		// indicates the max stack size for the method.
		// because we used the COMPUTE_FRAMES parameter in the classwriter
		// constructor, asm
		// will do this for us. The parameters to visitMaxs don't matter, but
		// the method must
		// be called.
		mv.visitMaxs(1, 1);
		// finish up code generation for this method.
		mv.visitEnd();
		// end of constructor

		// create main method which does the following
		// 1. instantiate an instance of the class being generated, passing the
		// String[] with command line arguments
		// 2. invoke the run method.
		mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
		mv.visitCode();
		Label mainStart = new Label();
		mv.visitLabel(mainStart);
		// this is for convenience during development--you can see that the code
		// is doing something.
		CodeGenUtils.genPrint(DEVEL, mv, "\nentering main");
		mv.visitTypeInsn(NEW, className);
		mv.visitInsn(DUP);
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESPECIAL, className, "<init>", "([Ljava/lang/String;)V", false);
		mv.visitMethodInsn(INVOKEVIRTUAL, className, "run", "()V", false);
		mv.visitInsn(RETURN);
		Label mainEnd = new Label();
		mv.visitLabel(mainEnd);
		mv.visitLocalVariable("args", "[Ljava/lang/String;", null, mainStart, mainEnd, 0);
		mv.visitLocalVariable("instance", classDesc, null, mainStart, mainEnd, 1);
		mv.visitMaxs(0, 0);
		mv.visitEnd();

		// create run method
		mv = cw.visitMethod(ACC_PUBLIC, "run", "()V", null, null);
		mv.visitCode();
		Label startRun = new Label();
		mv.visitLabel(startRun);
		CodeGenUtils.genPrint(DEVEL, mv, "\nentering run");
		program.getB().visit(this, new Integer(1));
		CodeGenUtils.genPrint(DEVEL, mv, "\n");
		mv.visitInsn(RETURN);
		Label endRun = new Label();
		mv.visitLabel(endRun);
		mv.visitLocalVariable("this", classDesc, null, startRun, endRun, 0);
		// TODO visit the local variables
		mv.visitMaxs(1, 1);
		mv.visitEnd(); // end of run method

		cw.visitEnd();// end of class

		// generate classfile and return it
		return cw.toByteArray();
	}

	@Override
	public Object visitAssignmentStatement(AssignmentStatement assignStatement, Object arg) throws Exception {
		assignStatement.getE().visit(this, arg);
		if(assignStatement.getE().getTypeName().isType(IMAGE, FRAME)) {
			;
			
		} else {
			CodeGenUtils.genPrint(DEVEL, mv, "\nassignment: " + assignStatement.var.getText() + "=");
			CodeGenUtils.genPrintTOS(GRADE, mv, assignStatement.getE().getType());
		}
		assignStatement.getVar().visit(this, arg);
		
		return null;
	}

	@Override
	public Object visitBinaryChain(BinaryChain binaryChain, Object arg) throws Exception {
		Chain c0 = binaryChain.getE0();
		ChainElem c1 = binaryChain.getE1();
		Token op = binaryChain.getArrow();
		ArrayList<Object> tmpArrLeft = new ArrayList<Object>();
		tmpArrLeft.add("left");
		tmpArrLeft.add(op);
		ArrayList<Object> tmpArrRight = new ArrayList<Object>();
		tmpArrRight.add("right");
		tmpArrRight.add(op);
		c0.visit(this, tmpArrLeft);
		c1.visit(this, tmpArrRight);
		
		return null;
	}

	@Override
	public Object visitBinaryExpression(BinaryExpression binaryExpression, Object arg) throws Exception {
		// TODO Implement this
		Expression e0 = binaryExpression.getE0();
		Expression e1 = binaryExpression.getE1();
		Token op = binaryExpression.getOp();
		e0.visit(this, null);
		e1.visit(this, null);

		if(op.isKind(AND, OR)) {
			if (e0.getTypeName() == TypeName.BOOLEAN && e1.getTypeName() == TypeName.BOOLEAN) {
				if(op.isKind(AND)) {
					mv.visitInsn(IAND);
				} else if(op.isKind(OR)) {
					mv.visitInsn(IOR);
				}
			}
		}
		else if (op.isKind(PLUS, MINUS)) {
			if (e0.getTypeName() == TypeName.INTEGER && e1.getTypeName() == TypeName.INTEGER) {
				if (op.isKind(PLUS)) {
					mv.visitInsn(IADD);
				} else if (op.isKind(MINUS)) {
					mv.visitInsn(ISUB);
				}
			}
			else if (e0.getTypeName() == TypeName.IMAGE && e1.getTypeName() == TypeName.IMAGE) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				if (op.isKind(PLUS)) {
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "add", PLPRuntimeImageOps.addSig, false);
				} else if (op.isKind(MINUS)) {
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "sub", PLPRuntimeImageOps.subSig, false);
				}
			}
		} else if (op.isKind(TIMES, DIV, MOD)) {
			if (e0.getTypeName() == TypeName.INTEGER && e1.getTypeName() == TypeName.INTEGER) {
				if (op.isKind(TIMES)) {
					mv.visitInsn(IMUL);
				} else if (op.isKind(DIV)) {
					mv.visitInsn(IDIV);
				} else if (op.isKind(MOD)) {
					mv.visitInsn(IREM);
				}
			}
			else if (e0.getTypeName() == TypeName.IMAGE && e1.getTypeName() == TypeName.INTEGER) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				if (op.isKind(TIMES)) {
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "mul", PLPRuntimeImageOps.mulSig, false);
				} else if (op.isKind(DIV)) {
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "div", PLPRuntimeImageOps.divSig, false);
				} else if (op.isKind(MOD)) {
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "mod", PLPRuntimeImageOps.modSig, false);
				}
			}
			else if (e0.getTypeName() == TypeName.INTEGER && e1.getTypeName() == TypeName.IMAGE) {
				if(op.isKind(TIMES)) {
					CodeGenUtils.genPrint(DEVEL, mv, "\n");
					mv.visitInsn(SWAP);
					mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "mul", PLPRuntimeImageOps.mulSig, false);
				}
			}
		} else if (op.isKind(LT, GT, LE, GE)) {
			if (e0.getTypeName() == TypeName.INTEGER && e1.getTypeName() == TypeName.INTEGER
					|| e0.getTypeName() == TypeName.BOOLEAN && e1.getTypeName() == TypeName.BOOLEAN) {
				Label ltStartLabel = new Label();
				Label ltEndLabel = new Label();
				if (op.isKind(LT)) {
					mv.visitJumpInsn(IF_ICMPGE, ltStartLabel);
				} else if (op.isKind(GT)) {
					mv.visitJumpInsn(IF_ICMPLE, ltStartLabel);
				} else if (op.isKind(LE)) {
					mv.visitJumpInsn(IF_ICMPGT, ltStartLabel);
				} else if (op.isKind(GE)) {
					mv.visitJumpInsn(IF_ICMPLT, ltStartLabel);
				}
				mv.visitLdcInsn(true);
				mv.visitJumpInsn(GOTO, ltEndLabel);
				mv.visitLabel(ltStartLabel);
				mv.visitLdcInsn(false);
				mv.visitLabel(ltEndLabel);
				mv.visitInsn(NOP);
			}
		} else if (op.isKind(EQUAL, NOTEQUAL)) {
			if (e0.getTypeName() == e1.getTypeName()) {
				Label eqStartLabel = new Label();
				Label eqEndLabel = new Label();
				if (op.isKind(EQUAL)) {
					mv.visitJumpInsn(IF_ICMPNE, eqStartLabel);
				} else if (op.isKind(NOTEQUAL)) {
					mv.visitJumpInsn(IF_ICMPEQ, eqStartLabel);
				}
				mv.visitLdcInsn(true);
				mv.visitJumpInsn(GOTO, eqEndLabel);
				mv.visitLabel(eqStartLabel);
				mv.visitLdcInsn(false);
				mv.visitLabel(eqEndLabel);
				mv.visitInsn(NOP);
			}
		} else {
			assert false : "Unsupported operation in BinaryExpression.";
		}

		return null;
	}

	@Override
	public Object visitBlock(Block block, Object arg) throws Exception {
		// TODO Implement this
		ArrayList<Dec> decs = block.getDecs();
		for (int i = 0; i < decs.size(); i++) {
			Dec dec = decs.get(i);
			dec.setSlot(i + (Integer) arg);
			maxSlot = Math.max(maxSlot, i + (Integer) arg);
		}

		Label startDec = new Label();
		mv.visitLabel(startDec);
		// initially 0 assigned to all decs
		for(Dec dec : decs) {
			if(dec.getTypeName().isType(TypeName.INTEGER, TypeName.BOOLEAN)) {
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, dec.getSlot());
			}
			else if(dec.getTypeName().isType(TypeName.FRAME)) {
				mv.visitInsn(ACONST_NULL);
				mv.visitVarInsn(ASTORE, dec.getSlot());
			}
		}
		//
		ArrayList<Statement> stats = block.getStatements();
		for (Statement stat : stats) {
			stat.visit(this, null);
			if(stat.getClass().equals(BinaryChain.class)) {
				mv.visitInsn(POP);
			}
		}
		Label endDec = new Label();
		mv.visitLabel(endDec);
		ArrayList<Object> tmpArr = new ArrayList<Object>();
		tmpArr.add(startDec);
		tmpArr.add(endDec);
		for (int i = 0; i < decs.size(); i++) {
			Dec dec = decs.get(i);
			tmpArr.add(new Integer(dec.getSlot()));
			dec.visit(this, tmpArr);
			tmpArr.remove(2);
		}

		return null;
	}

	@Override
	public Object visitBooleanLitExpression(BooleanLitExpression booleanLitExpression, Object arg) throws Exception {
		// TODO Implement this
		mv.visitLdcInsn(booleanLitExpression.getValue());
		return null;
	}

	@Override
	public Object visitConstantExpression(ConstantExpression constantExpression, Object arg) {
		Token t = constantExpression.getFirstToken();
		CodeGenUtils.genPrint(DEVEL, mv, "\n");
		if(t.isKind(Kind.KW_SCREENWIDTH)) {
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFrame.JVMClassName, "getScreenWidth", PLPRuntimeFrame.getScreenWidthSig, false);
		}
		else if(t.isKind(Kind.KW_SCREENHEIGHT)) {
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFrame.JVMClassName, "getScreenHeight", PLPRuntimeFrame.getScreenHeightSig, false);
		}
		return null;
	}

	@Override
	public Object visitDec(Dec declaration, Object arg) throws Exception {
		// TODO Implement this
		String localVarType = "";
		if (declaration.getTypeName() == TypeName.BOOLEAN) {
			localVarType = "Z";
		} else if (declaration.getTypeName() == TypeName.INTEGER) {
			localVarType = "I";
		} else if (declaration.getTypeName() == TypeName.IMAGE) {
			localVarType = PLPRuntimeImageIO.BufferedImageDesc;
		} else if (declaration.getTypeName() == TypeName.FRAME) {
			localVarType = PLPRuntimeFrame.JVMDesc;
		}
		else {
			return null;
		}
		ArrayList<Object> tmpArr = (ArrayList<Object>) arg;
		Label startDec = (Label) tmpArr.get(0);
		Label endDec = (Label) tmpArr.get(1);
		
		Integer myInt = (Integer) tmpArr.get(2);
		mv.visitLocalVariable(declaration.getIdent().getText(), localVarType, null, startDec, endDec, myInt);
	
		return null;
	}

	@Override
	public Object visitFilterOpChain(FilterOpChain filterOpChain, Object arg) throws Exception {
		ArrayList<Object> tmpArr = (ArrayList<Object>)arg;
		Token arrowToken = (Token)tmpArr.get(1);
		
		if(arrowToken.isKind(ARROW)) {
			mv.visitInsn(ACONST_NULL);
		}
		else if(arrowToken.isKind(BARARROW)) {
			mv.visitInsn(DUP);
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "copyImage", PLPRuntimeImageOps.copyImageSig, false);
			mv.visitInsn(SWAP);
		}
		CodeGenUtils.genPrint(DEVEL, mv, "\n");
		if(filterOpChain.getFirstToken().isKind(Kind.OP_GRAY)) {	
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFilterOps.JVMName, "grayOp", PLPRuntimeFilterOps.opSig, false);
		}
		else if(filterOpChain.getFirstToken().isKind(Kind.OP_CONVOLVE)) {
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFilterOps.JVMName, "convolveOp", PLPRuntimeFilterOps.opSig, false);
		}
		else if(filterOpChain.getFirstToken().isKind(Kind.OP_BLUR)) {
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFilterOps.JVMName, "blurOp", PLPRuntimeFilterOps.opSig, false);
		}
				
		return null;
	}

	@Override
	public Object visitFrameOpChain(FrameOpChain frameOpChain, Object arg) throws Exception {
		if(frameOpChain.getFirstToken().isKind(Kind.KW_SHOW)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeFrame.JVMClassName, "showImage", PLPRuntimeFrame.showImageDesc, false);
		}
		else if(frameOpChain.getFirstToken().isKind(Kind.KW_HIDE)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeFrame.JVMClassName, "hideImage", PLPRuntimeFrame.hideImageDesc, false);
		}
		else if(frameOpChain.getFirstToken().isKind(Kind.KW_MOVE)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			Tuple t = frameOpChain.getArg();
			t.visit(this, null);
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeFrame.JVMClassName, "moveFrame", PLPRuntimeFrame.moveFrameDesc, false);
		}
		else if(frameOpChain.getFirstToken().isKind(Kind.KW_XLOC)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeFrame.JVMClassName, "getXVal", PLPRuntimeFrame.getXValDesc, false);
			CodeGenUtils.genPrint(DEVEL, mv, " now top of stack: ");
			CodeGenUtils.genPrintTOS(DEVEL, mv, frameOpChain.getTypeName());
		}
		else if(frameOpChain.getFirstToken().isKind(Kind.KW_YLOC)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeFrame.JVMClassName, "getYVal", PLPRuntimeFrame.getYValDesc, false);
			CodeGenUtils.genPrint(DEVEL, mv, " now top of stack: ");
			CodeGenUtils.genPrintTOS(DEVEL, mv, frameOpChain.getTypeName());
		}
		
		return null;
	}

	@Override
	public Object visitIdentChain(IdentChain identChain, Object arg) throws Exception {
		ArrayList<Object> tmpArr = (ArrayList<Object>)arg;
		String lr = new String((String)tmpArr.get(0));
		Chain c = identChain;
		String fieldType = c.getTypeName().getJVMTypeDesc();
		if(lr.equals("left")) {
			if(c.getTypeName().isType(URL)) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				// load this
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, className, c.getFirstToken().getText(), fieldType);
				mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageIO.className, "readFromURL", PLPRuntimeImageIO.readFromURLSig, false);
			}
			else if(c.getTypeName().isType(TypeName.FILE)) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				// load this
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, className, c.getFirstToken().getText(), fieldType);
				mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageIO.className, "readFromFile", PLPRuntimeImageIO.readFromFileDesc, false);
			}
			else if(c.getTypeName().isType(IMAGE)) {
				Dec dec = identChain.getDec();
				mv.visitVarInsn(ALOAD, dec.getSlot());
			}
			else if(c.getTypeName().isType(FRAME)) {
				Dec dec = identChain.getDec();
				mv.visitVarInsn(ALOAD, dec.getSlot());
			}
			else if(c.getTypeName().isType(TypeName.INTEGER)) {
				Dec dec = identChain.getDec();
				mv.visitVarInsn(ILOAD, dec.getSlot());
			}
			
		}
		else if(lr.equals("right")) {
			if(c.getTypeName().isType(IMAGE)) {
				Dec dec = identChain.getDec();
				mv.visitInsn(DUP);
				mv.visitVarInsn(ASTORE, dec.getSlot());
			}
			else if(c.getTypeName().isType(FRAME)) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				Dec dec = identChain.getDec();
				mv.visitVarInsn(ALOAD, dec.getSlot());
				mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeFrame.JVMClassName, "createOrSetFrame", PLPRuntimeFrame.createOrSetFrameSig, false);
				mv.visitInsn(DUP);
				mv.visitVarInsn(ASTORE, dec.getSlot());
			}
			else if(c.getTypeName().isType(TypeName.FILE)) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				// load this
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, className, c.getFirstToken().getText(), fieldType);
				mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageIO.className, "write", PLPRuntimeImageIO.writeImageDesc, false);
			}
			else if(c.getTypeName().isType(TypeName.INTEGER)) {
				Dec dec = identChain.getDec();
				mv.visitInsn(DUP);
				mv.visitVarInsn(ISTORE, dec.getSlot());
			}
			
		}
		
		return null;
	}

	@Override
	public Object visitIdentExpression(IdentExpression identExpression, Object arg) throws Exception {
		// TODO Implement this
		Dec dec = identExpression.getDec();
		if (dec.getSlot() == -1) {
			// load this
			mv.visitVarInsn(ALOAD, 0);
			//
			String fieldType = "";
			if (dec.getTypeName() == TypeName.BOOLEAN) {
				fieldType = "Z";
			} else if (dec.getTypeName() == TypeName.INTEGER) {
				fieldType = "I";
			} else if (dec.getTypeName() == TypeName.FILE) {
				fieldType = TypeName.FILE.getJVMTypeDesc();
			}
			else if (dec.getTypeName() == TypeName.URL) {
				fieldType = TypeName.URL.getJVMTypeDesc();
			}
			else {
				assert false : "IdentExpression Type must be boolean or integer or file or url for paramDec.";
			}
			mv.visitFieldInsn(GETFIELD, className, dec.getIdent().getText(), fieldType);
		} else {
			if(dec.getTypeName().isType(TypeName.INTEGER, TypeName.BOOLEAN)) {
				mv.visitVarInsn(ILOAD, dec.getSlot());
			}
			else if(dec.getTypeName().isType(TypeName.IMAGE, TypeName.FRAME)) {
				mv.visitVarInsn(ALOAD, dec.getSlot());
			}
		}
		return null;
	}

	@Override
	public Object visitIdentLValue(IdentLValue identX, Object arg) throws Exception {
		// TODO Implement this
		Dec dec = identX.getDec();
		if (dec.getSlot() == -1) {
			// load this
			mv.visitVarInsn(ALOAD, 0);
			//
			mv.visitInsn(SWAP);
			String fieldType = "";
			if (dec.getTypeName() == TypeName.BOOLEAN) {
				fieldType = "Z";
			} else if (dec.getTypeName() == TypeName.INTEGER) {
				fieldType = "I";
			} else if (dec.getTypeName() == TypeName.FILE) {
				fieldType = TypeName.FILE.getJVMTypeDesc();
			}
			else if (dec.getTypeName() == TypeName.URL) {
				fieldType = TypeName.URL.getJVMTypeDesc();
			}
			else {
				assert false : "IdentLValue Type must be boolean or integer or file or url for paramDec.";
			}
			mv.visitFieldInsn(PUTFIELD, className, dec.getIdent().getText(), fieldType);
			
		} else {
			if(dec.getTypeName().isType(TypeName.INTEGER, TypeName.BOOLEAN)) {
				mv.visitVarInsn(ISTORE, dec.getSlot());
			}
			else if(dec.getTypeName().isType(TypeName.IMAGE)) {
				CodeGenUtils.genPrint(DEVEL, mv, "\n");
				mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "copyImage", PLPRuntimeImageOps.copyImageSig, false);
				mv.visitVarInsn(ASTORE, dec.getSlot());
			}
			else if(dec.getTypeName().isType(TypeName.FRAME)) {
				mv.visitVarInsn(ASTORE, dec.getSlot());
			}

		}
		return null;
	}

	@Override
	public Object visitIfStatement(IfStatement ifStatement, Object arg) throws Exception {
		// TODO Implement this
		Label ifLabel = new Label();
		Expression e = ifStatement.getE();
		Block b = ifStatement.getB();
		e.visit(this, null);
		mv.visitJumpInsn(IFEQ, ifLabel);
		b.visit(this, maxSlot + 1);
		mv.visitLabel(ifLabel);
		mv.visitInsn(NOP);
		return null;
	}

	@Override
	public Object visitImageOpChain(ImageOpChain imageOpChain, Object arg) throws Exception {
		if(imageOpChain.getFirstToken().isKind(Kind.KW_SCALE)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\n");
			Tuple t = imageOpChain.getArg();
			t.visit(this, null);
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageOps.JVMName, "scale", PLPRuntimeImageOps.scaleSig, false);
		}
		else if(imageOpChain.getFirstToken().isKind(Kind.OP_HEIGHT)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\nOP_HEIGHT");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeImageIO.BufferedImageClassName, "getHeight", PLPRuntimeImageOps.getHeightSig, false);
			CodeGenUtils.genPrint(DEVEL, mv, " now top of stack: ");
			CodeGenUtils.genPrintTOS(DEVEL, mv, imageOpChain.getTypeName());
		}
		else if(imageOpChain.getFirstToken().isKind(Kind.OP_WIDTH)) {
			CodeGenUtils.genPrint(DEVEL, mv, "\nOP_WIDTH");
			mv.visitMethodInsn(INVOKEVIRTUAL, PLPRuntimeImageIO.BufferedImageClassName, "getWidth", PLPRuntimeImageOps.getWidthSig, false);
			CodeGenUtils.genPrint(DEVEL, mv, " now top of stack: ");
			CodeGenUtils.genPrintTOS(DEVEL, mv, imageOpChain.getTypeName());
		}
		
		return null;
	}

	@Override
	public Object visitIntLitExpression(IntLitExpression intLitExpression, Object arg) throws Exception {
		// TODO Implement this
		mv.visitLdcInsn(intLitExpression.getValue());
		return null;
	}

	@Override
	public Object visitParamDec(ParamDec paramDec, Object arg) throws Exception {
		// TODO Implement this
		// For assignment 5, only needs to handle integers and booleans
		String fieldName = paramDec.getIdent().getText();
		String fieldType = "";
		Object initValue = null;
		FieldVisitor fv;
		TypeName hTypeName = paramDec.getTypeName();
		if (hTypeName.isType(TypeName.BOOLEAN)) {
			fieldType = "Z";
		} else if (hTypeName.isType(TypeName.INTEGER)) {
			fieldType = "I";
		} else if (hTypeName.isType(TypeName.URL)) {
			fieldType = hTypeName.getJVMTypeDesc();
		} else if (hTypeName.isType(TypeName.FILE)) {
			fieldType = hTypeName.getJVMTypeDesc();
		}
		else {
			assert false : "paramDec must be boolean or integer.";
		}

		fv = cw.visitField(ACC_PUBLIC, fieldName, fieldType, null, initValue);
		fv.visitEnd();

		if (hTypeName.isType(TypeName.BOOLEAN, TypeName.INTEGER)) {
			// load this
			mv.visitVarInsn(ALOAD, 0);
			// load string
			mv.visitVarInsn(ALOAD, 1);
			mv.visitIntInsn(BIPUSH, (Integer) arg);
			mv.visitInsn(AALOAD);
			if (hTypeName.isType(TypeName.BOOLEAN)) {
				// parseBoolean
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "parseBoolean", "(Ljava/lang/String;)Z", false);
			} else if (hTypeName.isType(TypeName.INTEGER)) {
				// parseInt
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "parseInt", "(Ljava/lang/String;)I", false);
			}
			mv.visitFieldInsn(PUTFIELD, className, fieldName, fieldType);	
		}
		else if (hTypeName.isType(TypeName.URL)) {
			mv.visitVarInsn(ALOAD, 1);
			mv.visitIntInsn(BIPUSH, (Integer)arg);
			mv.visitMethodInsn(INVOKESTATIC, PLPRuntimeImageIO.className, "getURL", PLPRuntimeImageIO.getURLSig, false);
			// load this
			mv.visitVarInsn(ALOAD, 0);
			//
			mv.visitInsn(SWAP);
			mv.visitFieldInsn(PUTFIELD, className, fieldName, fieldType);
		}
		else if (hTypeName.isType(TypeName.FILE)) {
			// new FILE
			String hClassName = hTypeName.getJVMClass();
			mv.visitTypeInsn(NEW, hClassName);
			mv.visitInsn(DUP);
			// load string
			mv.visitVarInsn(ALOAD, 1);
			mv.visitIntInsn(BIPUSH, (Integer) arg);
			mv.visitInsn(AALOAD);
			//
			mv.visitMethodInsn(INVOKESPECIAL, hClassName, "<init>", "(Ljava/lang/String;)V", false);
			// load this
			mv.visitVarInsn(ALOAD, 0);
			//
			mv.visitInsn(SWAP);
			mv.visitFieldInsn(PUTFIELD, className, fieldName, fieldType);
		}
		
		if (DEVEL && (hTypeName.isType(TypeName.INTEGER, TypeName.BOOLEAN))) {
			// load this
			mv.visitVarInsn(ALOAD, 0);
			//
			mv.visitFieldInsn(GETFIELD, className, fieldName, fieldType);
			CodeGenUtils.genPrint(DEVEL, mv, "\nparamDec: ");
			CodeGenUtils.genPrintTOS(DEVEL, mv, paramDec.getTypeName());
			CodeGenUtils.genPrint(DEVEL, mv, ", " + paramDec.getTypeName().toString());
			mv.visitInsn(POP);
		}

		return null;
	}

	@Override
	public Object visitSleepStatement(SleepStatement sleepStatement, Object arg) throws Exception {				
		Label startTry = new Label();
		Label endTry = new Label();
		Label startHandler = new Label();
		Label endHandler = new Label();
		
		mv.visitTryCatchBlock(startTry, endHandler, startHandler, "java/lang/InterruptedException");
		mv.visitLabel(startTry);
		Expression e = sleepStatement.getE();
		e.visit(this, null);
		mv.visitInsn(I2L);
		mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "sleep", "(J)V", false);
		mv.visitJumpInsn(GOTO, endHandler);
		mv.visitLabel(endTry);
		mv.visitLabel(startHandler);
		mv.visitVarInsn(ASTORE, maxSlot+1);
		mv.visitVarInsn(ALOAD, maxSlot+1);
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/InterruptedException", "printStackTrace", "()V", false);
		mv.visitLabel(endHandler);
		mv.visitLocalVariable("e", "Ljava/lang/InterruptedException;", null, startHandler, endHandler, maxSlot+1);
		
		return null;
	}

	@Override
	public Object visitTuple(Tuple tuple, Object arg) throws Exception {
		List<Expression> arr = tuple.getExprList();
		for(Expression exp: arr) {
			exp.visit(this, null);
		}
		return null;
	}

	@Override
	public Object visitWhileStatement(WhileStatement whileStatement, Object arg) throws Exception {
		// TODO Implement this
		Label bodyLabel = new Label();
		Label guardLabel = new Label();
		Expression e = whileStatement.getE();
		Block b = whileStatement.getB();
		mv.visitJumpInsn(GOTO, guardLabel);
		mv.visitLabel(bodyLabel);
		b.visit(this, maxSlot + 1);
		mv.visitLabel(guardLabel);
		e.visit(this, null);
		mv.visitJumpInsn(IFNE, bodyLabel);
		return null;
	}

}
