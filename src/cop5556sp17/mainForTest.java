package cop5556sp17;

import cop5556sp17.Parser.SyntaxException;
import cop5556sp17.Scanner.IllegalCharException;
import cop5556sp17.Scanner.IllegalNumberException;

import java.io.FileOutputStream;
import java.io.OutputStream;

import cop5556sp17.AST.*;


public class mainForTest {	
//	// Scanner Test
//	public static void main(String[] args) throws IllegalCharException, IllegalNumberException {
//		// TODO Auto-generated method stub
//		String [] reservedList = {"integer", "boolean", "image", "url", "file", "frame", "while", "if", "sleep", "screenheight", "screenwidth",
//				"true", "false", "gray", "convolve", "blur", "scale", "width", "height", "xloc", "yloc", "hide", "show", "move"};
//		String joined = String.join(",", reservedList);
//		String input = "  |-|->  ";
////		input = "/*...*/a/***/\nbc!/ /*/ /**/ !\nd/*.**/";
////		input = "/* * ** */\nabc";
//		input = "321 abc /*...*/ z /*..*/a/***/\n a /*.*/ bc /*.*/ bc!/ /*/ /**/ !\nd/*.**/";
//		String input2 = " integer myint; |-> -> <- || && & |;\n"
//				+ " boolean mybool; /* comment */ 5 / 2 ;\n"
//				+ "scale file true 5+3 032 * 320 ; ; ;;\n";
//		
//		Scanner scanner = new Scanner(input);
//		scanner.scan();
////		System.out.println("");
////		System.out.println("");
////		Scanner.Token t = scanner.nextToken();
////		while(t != null) {
////			System.out.println(t +" " + t.getText());
////			t = scanner.nextToken();
////		}
//	}
//	// Parser Test
//	public static void main(String[] args) throws IllegalCharException, IllegalNumberException, SyntaxException {
//		// TODO Auto-generated method stub
//		
//		String input = "  |-|->  ";
//		input = "prog0 integer sebo {sleep 5; sleep(5*3-2);}";
//		input = "sebo -> convolve (4*3+2) |-> move -> blur -> gray (4*2+2, 5/2%seb) |-> show -> "
//				+ "hide -> move -> xloc -> yloc -> width -> height -> scale ";
//		
//		
//		Parser parser = new Parser(new Scanner(input).scan());
////		parser.parse();
//		parser.chain(); parser.matchEOF();
//	}
//	// AST Test
//	public static void main(String[] args) throws IllegalCharException, IllegalNumberException, SyntaxException {
//		// TODO Auto-generated method stub
//		
//		String input = "  |-|->  ";
//		input = "prog0 integer sebo {integer sebo2 sleep sebo2;}";
////		input = "(1+2, 3*5, 3%2)";
//		Parser parser = new Parser(new Scanner(input).scan());
//		parser.parse();
////		parser.arg();
//	}
//	// Type checking Test
//	public static void main(String[] args) throws IllegalCharException, IllegalNumberException, SyntaxException, Exception {
//		// TODO Auto-generated method stub
//		String input = "p {\nboolean y \ny <- false;}";
//		input = "p file myfile, url myurl, url myfile {boolean b integer a1 integer a2 image i1 image i2 image i3 "
//				+ "boolean a1}";
//		Parser parser = new Parser(new Scanner(input).scan());
//		ASTNode program = parser.parse();
//		TypeCheckVisitor v = new TypeCheckVisitor();
//		program.visit(v, null);
//	}
	// Code Generation Test
	public static void main(String[] args) throws IllegalCharException, IllegalNumberException, SyntaxException, Exception {
		
		// TODO Auto-generated method stub
		boolean devel = false;
		boolean grade = true;
		if (devel || grade) PLPRuntimeLog.initLog();
		String input = "p {\nboolean y \ny <- false;}";
//		input = "p file myfile, url myurl { integer i1 integer i2 integer i3 image myimage image myimage2 image myimage3 image myimage4 frame myframe frame myframe2 "
//				+ "myfile->myimage; myurl->myimage2;"
//				+ "while(i1<100) {myimage|->convolve; i1<-i1+1;}"
//				+ "myimage->myframe->show; "
//				+ "i1<-0;"
//				+ "while(i1<50) {"
//				+ "i2<-i1*10; myframe->move(i2,i2); i1<- i1+1; sleep(50);"
//				+ "}"
//				+ ""
//				+ "}";;
		
		input = "p file myfile, url myurl { integer i1 integer i2 integer i3 image myimage image myimage2 image myimage3 image myimage4 frame myframe frame myframe2 "
				+ "myfile->myimage; myurl->myimage2;"
				+ "integer const const<-20; myimage->myframe->move(const,const)->show; integer xloci1 myimage->width->xloci1; integer yloci1 myimage->height->yloci1;"
				+ "i1<-0; myimage3<-myimage2; "
				+ "integer duration duration<-100;"
				+ "image myimage1a myimage1a<-myimage; "
				+ "myimage3->myframe2->move(xloci1+const, yloci1+const);"
				+ "while(i1<50) {"
				+ "		if(i1%2==0) {"
				+ "			myimage1a|->convolve|->convolve;"
				+ "			myimage3|->blur|->blur;"
				+ "		}"
				+ "		if(i1%2==1) {"
				+ "			myimage1a|->blur;"
				+ "			myimage3|->convolve;"		
				+ "		}"
				+ "		myimage3->myframe2->show; "
				+ "		myimage1a->myframe->show; i1<-i1+1; sleep(duration);"
				+ "} "
				+ ""
				+ "}";
//		input = "barArrowGray url u {image i frame f \nu -> i |-> gray -> f -> show;\n}";
//		input = "p url myurl { integer i1 integer i2 integer i3 image myimage image myimage2 image myimage3 image myimage4 frame myframe frame myframe2 "
//				+ " myurl->myimage;"
//				+ "myimage|->gray; myimage->myframe->show;"
//				+ "}";
		Parser parser = new Parser(new Scanner(input).scan());
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
		CodeGenVisitor cv = new CodeGenVisitor(devel, grade, null);
		byte[] bytecode = (byte[]) program.visit(cv, null);
		System.out.println(program);
		// output the generated bytecode
		CodeGenUtils.dumpBytecode(bytecode);
		//write byte code to file 
		String name = ((Program) program).getName();
		String classFileName = "bin/" + name + ".class";
		OutputStream output = new FileOutputStream(classFileName);
		output.write(bytecode);
		output.close();
		System.out.println("wrote classfile to " + classFileName);
		// directly execute bytecode
//		String[] args2 = new String[]{"9", "9", "0", "true"}; //create command line argument array to initialize params, none in this case
//		String[] args2 = new String[]{"https://lh3.googleusercontent.com/gJIMHUXRr8fAUruzLFBh6VaXpYS-V7gU1SCWaWIWlTCKEpQbcdQiXIRU2mJMpRZmUFg=w300"}; //create command line argument array to initialize params, none in this case
//		String[] args2 = new String[]{"abc.png", "http://cdn.thecoolist.com/wp-content/uploads/2016/05/Japanese-Cherry-beautiful-tree-960x540.jpg"}; //create command line argument array to initialize params, none in this case
		String[] args2 = new String[]{"file:/Users/sanari85/eclipse/workspace/myCompiler/abc.png"}; //create command line argument array to initialize params, none in this case
		
		Runnable instance = CodeGenUtils.getInstance(name, bytecode, args2);
		instance.run();
		System.out.println(PLPRuntimeLog.getString());
	}

}
