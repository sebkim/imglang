/**  Important to test the error cases in case the
 * AST is not being completely traversed.
 * 
 * Only need to test syntactically correct programs, or
 * program fragments.
 */

package cop5556sp17;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cop5556sp17.AST.ASTNode;
import cop5556sp17.AST.Dec;
import cop5556sp17.AST.IdentExpression;
import cop5556sp17.AST.Program;
import cop5556sp17.AST.Statement;
import cop5556sp17.Parser.SyntaxException;
import cop5556sp17.Scanner.IllegalCharException;
import cop5556sp17.Scanner.IllegalNumberException;
import cop5556sp17.TypeCheckVisitor.TypeCheckException;

public class TypeCheckVisitorTest {
	

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testAssignmentBoolLit0() throws Exception{
		String input = "p {\nboolean y \ny <- false;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);		
	}

	@Test
	public void testAssignmentBoolLitError0() throws Exception{
		String input = "p {\nboolean y \ny <- 3;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testSleep0() throws Exception{
		String input = "p {integer intsebo integer intsebo2 integer intsebo3 sleep intsebo+intsebo2*intsebo3/intsebo*screenwidth/screenheight;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);		
	}
	@Test
	public void testSleepError0() throws Exception{
		String input = "p {boolean boolsebo sleep boolsebo;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testSleepError1() throws Exception{
		String input = "p {boolean boolsebo sleep intsebo;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testSleepError2() throws Exception{
		String input = "p {integer a integer b sleep a<b;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testSleepError3() throws Exception{
		String input = "p {integer a integer b sleep a+b*c;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testAssign0() throws Exception{
		String input = "p boolean sebo {integer a integer b sebo <- a<b;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);		
	}
	@Test
	public void testAssign1() throws Exception{
		String input = "p boolean sebo {integer a integer b sebo <- a!=b;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);		
	}
	@Test
	public void testAssignError0() throws Exception{
		String input = "p integer sebo {sebo <- true;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testAssignError1() throws Exception{
		String input = "p integer sebo {sebo2 <- 5;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testIdentChainError0() throws Exception{
		String input = "p integer sebo {image im sebo2 -> im;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testWhile0() throws Exception{
		String input = "p boolean sebo {integer a integer b while(true) {}}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);		
	}
	@Test
	public void testWhileError0() throws Exception{
		String input = "p boolean sebo {integer a integer b while(a) {}}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);		
	}
	@Test
	public void testNestedScope0() throws Exception{
		String input = "p boolean sebo {integer a while(true) { image a a->convolve; } }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testNestedScopeError0() throws Exception{
		String input = "p boolean sebo {integer a while(true) { image a a->convolve; } a->convolve; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain0() throws Exception{
		String input = "p file myfile, url myurl { image myimage myurl->myimage; myfile->convolve; myurl->scale(5); }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain1() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->xloc; myframe->yloc; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain2() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->show->hide->move(1,2); }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain3() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage->width; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain4() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage->myframe->xloc; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain5() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage->myfile; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain6() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage->gray|->blur->convolve; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChain7() throws Exception{
		String input = "p file myfile, url myurl { if(true) {integer myint myint<- 3; image myimage frame myframe myimage->scale(myint)->myurl; }}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError0() throws Exception{
		String input = "p file myfile, url myurl { myurl -> myfile;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError1() throws Exception{
		String input = "p file myfile, url myurl { myfile -> myurl;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError2() throws Exception{
		String input = "p file myfile, url myurl { frame myframe myframe->width;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError3() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage->show;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError4() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myurl->myimage->move(1,2);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError5() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myfile->myimage->xloc;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryChainError6() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myimage|->gray->yloc;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testTupleError0() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->move(true,false);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testTupleError2() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->move(true,1);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testTupleError3() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->move(1,false);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testTupleError4() throws Exception{
		String input = "p file myfile, url myurl { image myimage frame myframe myframe->move(1,2,3);}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	@Test
	public void testBinaryExpression0() throws Exception{
		String input = "p file myfile, url myurl {integer a1 integer a2 image i1 image i2 image i3 "
				+ "sleep (a1-a2); i3 <- i1+i2; i3<-i1-i2; sleep a1+a2*a1/a2; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryExpression1() throws Exception{
		String input = "p file myfile, url myurl {integer a1 integer a2 image i1 image i2 image i3 "
				+ "i3 <- a1 * i1; i3 <- i1*a2; }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testBinaryExpression2() throws Exception{
		String input = "p file myfile, url myurl {boolean b integer a1 integer a2 image i1 image i2 image i3 "
				+ "b <- 1<a2; b<- true>=false; b<- false>=false; b<-a1==a2;}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testOverall0() throws Exception{
		String input = "p file myfile, file myfile2, url myurl, url myurl2 {boolean b1 boolean b2 integer a1 integer a2 image i1 image i2 frame f1 frame f2 "
				+ "sleep (a1*5+a2)*a1/a2-23; "
				+ "b1<-myfile!=myfile2; b1<-myurl==myurl2; i1<-i1+i2; "
				+ "if(f1==f2) { integer i1 integer i2 sleep i1*i2; } "
				+ "while(myurl!=myurl2) { boolean i1 boolean i2 i1<-i2; i1<-(a1*52/2)<123>false;}"
				+ "myfile->i1->height;"
				+ "myfile2->i2->gray->blur->convolve->scale(a1*a2/a1)->b2;"
				+ "myurl2->i2;"
				+ "f1->xloc; f2->yloc;"
				+ "f1->show->hide->move(1,2)->show;"
				+ "i1->f1->move(3,4)->show;"
				+ "i2->myfile;"
				+ "scale(100)->width;"
				+ "scale(200)->height;"
				+ "}";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		program.visit(v, null);
	}
	@Test
	public void testTwoParamsError() throws Exception{
		String input = "p file myfile, url myurl {boolean b integer a1 integer a2 image i1 image i2 image i3 "
				+ "boolean a2 }";
		Scanner scanner = new Scanner(input);
		scanner.scan();
		Parser parser = new Parser(scanner);
		ASTNode program = parser.parse();
		TypeCheckVisitor v = new TypeCheckVisitor();
		thrown.expect(TypeCheckVisitor.TypeCheckException.class);
		program.visit(v, null);
	}
	
	
}
