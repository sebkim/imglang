package cop5556sp17;


import cop5556sp17.AST.Dec;
import cop5556sp17.Parser.Pair;

import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;

public class SymbolTable {
	public static class Pair<L,R> {
	  private final L left;
	  private final R right;
	  public Pair(L left, R right) {
	    this.left = left;
	    this.right = right;
	  }
	  public L getLeft() { return left; }
	  public R getRight() { return right; }

	  @Override
	  public int hashCode() { return left.hashCode() ^ right.hashCode(); }

	  @Override
	  public boolean equals(Object o) {
	    if (!(o instanceof Pair)) return false;
	    Pair pairo = (Pair) o;
	    return this.left.equals(pairo.getLeft()) &&
	           this.right.equals(pairo.getRight());
	  }
	}
	
	//TODO  add fields
	int current_scope, next_scope;
	HashMap<String, Stack<Pair<Integer, Dec>>> table;
	Stack<Integer> scope_stack;
	
	/** 
	 * to be called when block entered
	 */
	public void enterScope(){
		//TODO:  IMPLEMENT THIS
		this.current_scope = this.next_scope++;
		this.scope_stack.push(this.current_scope);
	}
	
	
	/**
	 * leaves scope
	 */
	public void leaveScope(){
		//TODO:  IMPLEMENT THIS
		this.scope_stack.pop();
		this.current_scope = this.scope_stack.peek();
	}
	
	public boolean insert(String ident, Dec dec){
		//TODO:  IMPLEMENT THIS
		Stack<Pair<Integer, Dec>> hStack = this.table.get(ident);
		Pair<Integer, Dec> newPair = new Pair<Integer, Dec>(this.current_scope, dec);
		if(hStack == null) {
			Stack<Pair<Integer, Dec>> newStack = new Stack<Pair<Integer, Dec>>();
			newStack.push(newPair);
			this.table.put(ident, newStack);
			return true;
		} else {
			Integer recentScope = hStack.peek().getLeft();
			Dec recentDec = hStack.peek().getRight();
			if(recentScope.equals(this.current_scope) && recentDec.getIdent().getText().equals(dec.getIdent().getText())) {
				return false;
			} else {
				hStack.push(newPair);
				this.table.put(ident, hStack);
				return true;
			}
		}
	}
	
	public Dec lookup(String ident){
		//TODO:  IMPLEMENT THIS
		Stack<Pair<Integer, Dec>> hStack = this.table.get(ident);
		if(hStack==null) return null;
		else {
			Stack<Pair<Integer, Dec>> reversedStack = (Stack<Pair<Integer, Dec>>)hStack.clone();
			Collections.reverse(reversedStack);
			for(Pair<Integer, Dec> p: reversedStack) {
				if(p.getLeft()<=this.current_scope) {
					return p.getRight();
				}
			}
			return null;
		}
	}
		
	public SymbolTable() {
		//TODO:  IMPLEMENT THIS
		this.current_scope = 0;
		this.next_scope = 1;
		table = new HashMap<String, Stack<Pair<Integer, Dec>>>();
		scope_stack = new Stack<Integer>();
		scope_stack.push(0);
	}


	@Override
	public String toString() {
		//TODO:  IMPLEMENT THIS
		String hStr = "";
		for(String key: this.table.keySet()) {
			hStr += key;
			hStr += ": ";
			Stack<Pair<Integer, Dec>> hStack = this.table.get(key);
			Stack<Pair<Integer, Dec>> reversedStack = (Stack<Pair<Integer, Dec>>)hStack.clone();
			Collections.reverse(reversedStack);
			for(Pair<Integer, Dec> p: reversedStack) {
				hStr += String.format("(%d, %s, %s) -> ", p.getLeft(), p.getRight().getIdent().getText(), p.getRight().getType().getText());
			}
			hStr += "\n"; 
		}
		return hStr;
	}
	
}
