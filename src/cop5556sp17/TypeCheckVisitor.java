package cop5556sp17;

import cop5556sp17.AST.ASTNode;
import cop5556sp17.AST.ASTVisitor;
import cop5556sp17.AST.Tuple;
import cop5556sp17.AST.AssignmentStatement;
import cop5556sp17.AST.BinaryChain;
import cop5556sp17.AST.BinaryExpression;
import cop5556sp17.AST.Block;
import cop5556sp17.AST.BooleanLitExpression;
import cop5556sp17.AST.Chain;
import cop5556sp17.AST.ChainElem;
import cop5556sp17.AST.ConstantExpression;
import cop5556sp17.AST.Dec;
import cop5556sp17.AST.Expression;
import cop5556sp17.AST.FilterOpChain;
import cop5556sp17.AST.FrameOpChain;
import cop5556sp17.AST.IdentChain;
import cop5556sp17.AST.IdentExpression;
import cop5556sp17.AST.IdentLValue;
import cop5556sp17.AST.IfStatement;
import cop5556sp17.AST.ImageOpChain;
import cop5556sp17.AST.IntLitExpression;
import cop5556sp17.AST.ParamDec;
import cop5556sp17.AST.Program;
import cop5556sp17.AST.SleepStatement;
import cop5556sp17.AST.Statement;
import cop5556sp17.AST.Type.TypeName;
import cop5556sp17.AST.WhileStatement;

import java.util.ArrayList;
import java.util.List;

import cop5556sp17.Scanner.Kind;
import cop5556sp17.Scanner.LinePos;
import cop5556sp17.Scanner.Token;
import static cop5556sp17.AST.Type.TypeName.*;
import static cop5556sp17.Scanner.Kind.ARROW;
import static cop5556sp17.Scanner.Kind.KW_HIDE;
import static cop5556sp17.Scanner.Kind.KW_MOVE;
import static cop5556sp17.Scanner.Kind.KW_SHOW;
import static cop5556sp17.Scanner.Kind.KW_XLOC;
import static cop5556sp17.Scanner.Kind.KW_YLOC;
import static cop5556sp17.Scanner.Kind.OP_BLUR;
import static cop5556sp17.Scanner.Kind.OP_CONVOLVE;
import static cop5556sp17.Scanner.Kind.OP_GRAY;
import static cop5556sp17.Scanner.Kind.OP_HEIGHT;
import static cop5556sp17.Scanner.Kind.OP_WIDTH;
import static cop5556sp17.Scanner.Kind.*;

public class TypeCheckVisitor implements ASTVisitor {
	final boolean outDetail = false; 
	@SuppressWarnings("serial")
	public static class TypeCheckException extends Exception {
		TypeCheckException(String message) {
			super(message);
		}
	}

	SymbolTable symtab = new SymbolTable();

	@Override
	public Object visitBinaryChain(BinaryChain binaryChain, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Object retObj = null;
		Chain e0 = binaryChain.getE0();
		TypeName tn0 = (TypeName)e0.visit(this, null);
		ChainElem e1 = binaryChain.getE1();
		TypeName tn1 = (TypeName)e1.visit(this, null);
		Token op = binaryChain.getArrow();
		if (op.kind == Kind.ARROW || op.kind == Kind.BARARROW) {
			if(tn0 == TypeName.IMAGE && 
					(e1 instanceof FilterOpChain) && e1.getFirstToken().isKind(Kind.OP_GRAY, Kind.OP_BLUR, Kind.OP_CONVOLVE)) {
				binaryChain.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			}
		}
		if(retObj ==null && op.kind== Kind.ARROW) {
			if(tn0 == TypeName.URL && tn1 == TypeName.IMAGE) {
				binaryChain.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			} else if(tn0 == TypeName.FILE && tn1 == TypeName.IMAGE) {
				binaryChain.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			} else if(tn0 == TypeName.FRAME && 
					(e1 instanceof FrameOpChain) && e1.getFirstToken().isKind(Kind.KW_XLOC, Kind.KW_YLOC)) {
				binaryChain.setTypeName(TypeName.INTEGER);
				retObj = TypeName.INTEGER;
			} else if(tn0 == TypeName.FRAME &&
					(e1 instanceof FrameOpChain) && e1.getFirstToken().isKind(Kind.KW_SHOW, Kind.KW_HIDE, Kind.KW_MOVE)) {
				binaryChain.setTypeName(TypeName.FRAME);
				retObj = TypeName.FRAME;
			} else if(tn0== TypeName.IMAGE &&
					(e1 instanceof ImageOpChain) && e1.getFirstToken().isKind(OP_WIDTH, OP_HEIGHT)) {
				binaryChain.setTypeName(TypeName.INTEGER);
				retObj = TypeName.INTEGER;
			} else if(tn0 == TypeName.IMAGE && tn1 == TypeName.FRAME) {
				binaryChain.setTypeName(TypeName.FRAME);
				retObj = TypeName.FRAME;
			} else if(tn0 == TypeName.IMAGE && tn1 == TypeName.FILE) {
				binaryChain.setTypeName(TypeName.NONE);
				retObj = TypeName.NONE;
			} else if(tn0==TypeName.IMAGE && 
					(e1 instanceof ImageOpChain) && e1.getFirstToken().isKind(Kind.KW_SCALE)) {
				binaryChain.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			} else if(tn0 == TypeName.IMAGE && 
					(e1 instanceof IdentChain) && e1.getTypeName().isType(IMAGE)) {
				binaryChain.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			} else if(tn0 == TypeName.INTEGER && 
					(e1 instanceof IdentChain) && e1.getTypeName().isType(INTEGER)) {
				binaryChain.setTypeName(TypeName.INTEGER);
				retObj = TypeName.INTEGER;
			}
		}
		if(this.outDetail) {
			System.out.println("in visitBinaryChain");
			System.out.println(binaryChain);
			System.out.println("--------------");
		}
		if(retObj!=null) return retObj;
		throw new TypeCheckException(String.format("BinaryChain error at %s. (%s)", 
				binaryChain.getFirstToken().getLinePos(), binaryChain.getFirstToken().getText()));
	}

	@Override
	public Object visitBinaryExpression(BinaryExpression binaryExpression, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Object retObj = null;
		Expression e0 = binaryExpression.getE0();
		TypeName tn0 = (TypeName)e0.visit(this, null);
		Expression e1 = binaryExpression.getE1();
		TypeName tn1 = (TypeName)e1.visit(this, null);
		Token op = binaryExpression.getOp();
		
		if(op.kind == Kind.AND || op.kind == Kind.OR) {
			if(tn0 == TypeName.BOOLEAN && tn1 == TypeName.BOOLEAN) {
				binaryExpression.setTypeName(TypeName.BOOLEAN);
				retObj = TypeName.BOOLEAN;
			}
		}
		else if(op.kind == Kind.PLUS || op.kind == Kind.MINUS) {
			if(tn0 == TypeName.INTEGER && tn1 == TypeName.INTEGER) {
				binaryExpression.setTypeName(TypeName.INTEGER);
				retObj = TypeName.INTEGER;
			} else if(tn0 == TypeName.IMAGE && tn1 == TypeName.IMAGE) {
				binaryExpression.setTypeName(TypeName.IMAGE);
				retObj = TypeName.IMAGE;
			}
		} else if(op.kind == Kind.TIMES || op.kind == Kind.DIV || op.kind == Kind.MOD) {
			if(tn0 == TypeName.INTEGER && tn1 == TypeName.INTEGER) {
				binaryExpression.setTypeName(TypeName.INTEGER);
				retObj = TypeName.INTEGER;
			}
			if(retObj==null) {
				if(op.kind == Kind.TIMES) {
					if(tn0 == TypeName.INTEGER && tn1==TypeName.IMAGE) {
						binaryExpression.setTypeName(TypeName.IMAGE);
						retObj = TypeName.IMAGE;
					} else if(tn0 == TypeName.IMAGE && tn1 == TypeName.INTEGER) {
						binaryExpression.setTypeName(TypeName.IMAGE);
						retObj = TypeName.IMAGE;
					}
				}
				else if(op.kind == Kind.DIV) {
					if(tn0 == TypeName.IMAGE && tn1 == TypeName.INTEGER) {
						binaryExpression.setTypeName(TypeName.IMAGE);
						retObj = TypeName.IMAGE;
					}
				}
				else if(op.kind == Kind.MOD) {
					if(tn0 == TypeName.IMAGE && tn1 == TypeName.INTEGER) {
						binaryExpression.setTypeName(TypeName.IMAGE);
						retObj = TypeName.IMAGE;
					}
				}
				
			}
		} else if(op.kind == Kind.LT || op.kind == Kind.GT || op.kind == Kind.LE || op.kind == Kind.GE) {
			if(tn0 == TypeName.INTEGER && tn1 == TypeName.INTEGER) {
				binaryExpression.setTypeName(TypeName.BOOLEAN);
				retObj = TypeName.BOOLEAN;
			} else if(tn0 == TypeName.BOOLEAN && tn1 == TypeName.BOOLEAN) {
				binaryExpression.setTypeName(TypeName.BOOLEAN);
				retObj = TypeName.BOOLEAN;
			}
		} else if(op.kind == Kind.EQUAL || op.kind == Kind.NOTEQUAL) {
			if(tn0 == tn1) {
				binaryExpression.setTypeName(TypeName.BOOLEAN);
				retObj = TypeName.BOOLEAN;
			}
				
		}
		if(this.outDetail) {
			System.out.println("in visitBinaryExpression");
			System.out.println(binaryExpression);
			System.out.println("--------------");
		}
		if(retObj!=null) return retObj;
		throw new TypeCheckException(String.format("BinaryExpression error at %s. (%s)", 
				binaryExpression.getFirstToken().getLinePos(), binaryExpression.getFirstToken().getText()));
	}

	@Override
	public Object visitBlock(Block block, Object arg) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Dec> decs = block.getDecs();
		ArrayList<Statement> stats = block.getStatements();
		symtab.enterScope();
		if(this.outDetail) {
			System.out.println("in visitBlock");
			System.out.println(String.format("Enter scope. Now scope is %d.", symtab.current_scope));
			System.out.println("--------------");
		}
		for(Dec dec: decs) {
			dec.visit(this,  null);
		}
		for(Statement stat: stats) {
			stat.visit(this, null);
		}
		symtab.leaveScope();
		if(this.outDetail) {
			System.out.println("in visitBlock");
			System.out.println(String.format("Leave scope. Now scope is %d.", symtab.current_scope));
			System.out.println("--------------");
		}
		return null;
	}

	@Override
	public Object visitBooleanLitExpression(BooleanLitExpression booleanLitExpression, Object arg) throws Exception {
		// TODO Auto-generated method stub
		booleanLitExpression.setTypeName(TypeName.BOOLEAN);
		if(this.outDetail) {
			System.out.println("in visitBooleanLitExpression");
			System.out.println(booleanLitExpression);
			System.out.println("--------------");
		}
		return TypeName.BOOLEAN;
	}

	@Override
	public Object visitFilterOpChain(FilterOpChain filterOpChain, Object arg) throws Exception {
		// TODO Auto-generated method stub
		filterOpChain.setTypeName(TypeName.IMAGE);
		Tuple ht = filterOpChain.getArg();
		ht.visit(this, null);
		if(ht.getExprList().size()!=0) {
			throw new TypeCheckException(String.format("FilterOpChain error at %s. (%s) Tuple length should be 0." , 
					filterOpChain.getFirstToken().getLinePos(), filterOpChain.getFirstToken().getText()));
		}
		if(this.outDetail) {
			System.out.println("in visitFilterOpChain");
			System.out.println(filterOpChain);
			System.out.println("--------------");
		}
		return TypeName.IMAGE;
	}

	@Override
	public Object visitFrameOpChain(FrameOpChain frameOpChain, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Object retObj = null;
		Token firstToken = frameOpChain.getFirstToken();
		Tuple ht = frameOpChain.getArg();
		ht.visit(this, null);
		if(firstToken.isKind(Kind.KW_SHOW, Kind.KW_HIDE)) {
			if(ht.getExprList().size()!=0) {
				throw new TypeCheckException(String.format("SHOW or HIDE error at %s. (%s) Tuple length should be 0." , 
						frameOpChain.getFirstToken().getLinePos(), frameOpChain.getFirstToken().getText()));
			}
			frameOpChain.setTypeName(TypeName.NONE);
			retObj = TypeName.NONE;
		} else if(firstToken.isKind(Kind.KW_XLOC, Kind.KW_YLOC)) {
			if(ht.getExprList().size()!=0) {
				throw new TypeCheckException(String.format("XLOC or YLOC error at %s. (%s) Tuple length should be 0." , 
						frameOpChain.getFirstToken().getLinePos(), frameOpChain.getFirstToken().getText()));
			}
			frameOpChain.setTypeName(TypeName.INTEGER);
			retObj = TypeName.INTEGER;
		} else if (firstToken.isKind(Kind.KW_MOVE)) {
			if(ht.getExprList().size()!=2) {
				throw new TypeCheckException(String.format("MOVE error at %s. (%s) Tuple length should be 2." , 
						frameOpChain.getFirstToken().getLinePos(), frameOpChain.getFirstToken().getText()));
			}
			frameOpChain.setTypeName(TypeName.NONE);
			retObj = TypeName.NONE;
		}
		
		if(this.outDetail) {
			System.out.println("in visitFrameOpChain");
			System.out.println(frameOpChain);
			System.out.println("--------------");
		}
		if(retObj != null) return retObj;
		throw new TypeCheckException(String.format("FrameOpChain error at %s. (%s)", 
				frameOpChain.getFirstToken().getLinePos(), frameOpChain.getFirstToken().getText()));
	}

	@Override
	public Object visitIdentChain(IdentChain identChain, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Dec hDec = symtab.lookup(identChain.getFirstToken().getText());
		if(hDec==null) 
			throw new TypeCheckException(String.format("IdentChain error at %s. (%s) Variable is not declared before used." , 
					identChain.getFirstToken().getLinePos(), identChain.getFirstToken().getText()));
		else {
			identChain.setTypeName(hDec.getTypeName());
			identChain.setDec(hDec);
			if(this.outDetail) {
				System.out.println("in visitIdentChain");
				System.out.println(hDec);
				System.out.println("--------------");
			}
			return hDec.getTypeName();
		}
	}

	@Override
	public Object visitIdentExpression(IdentExpression identExpression, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Dec hDec = symtab.lookup(identExpression.getFirstToken().getText());
		if(hDec==null) 
			throw new TypeCheckException(String.format("IdentExpression error at %s. (%s) Variable is not declared before used." , 
					identExpression.getFirstToken().getLinePos(), identExpression.getFirstToken().getText()));
		else {
			identExpression.setTypeName(hDec.getTypeName());
			identExpression.setDec(hDec);
			if(this.outDetail) {
				System.out.println("in visitIdentExpression");
				System.out.println(hDec);
				System.out.println("--------------");
			}
			return hDec.getTypeName();
		}
	}

	@Override
	public Object visitIfStatement(IfStatement ifStatement, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Expression e = ifStatement.getE();
		TypeName tn = (TypeName)e.visit(this, null);
		if(tn != TypeName.BOOLEAN)
			throw new TypeCheckException(String.format("IfStatement error at %s. (%s) Type of exp in if should be boolean.", 
					ifStatement.getFirstToken().getLinePos(), ifStatement.getFirstToken().getText()));
		Block b = ifStatement.getB();
		b.visit(this, null);
		return null;
	}

	@Override
	public Object visitIntLitExpression(IntLitExpression intLitExpression, Object arg) throws Exception {
		// TODO Auto-generated method stub
		intLitExpression.setTypeName(TypeName.INTEGER);
		if(this.outDetail) {
			System.out.println("in visitIntLitExpression");
			System.out.println(intLitExpression);
			System.out.println("--------------");
		}
		return TypeName.INTEGER;
	}

	@Override
	public Object visitSleepStatement(SleepStatement sleepStatement, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Expression exp = sleepStatement.getE();
		TypeName tn = (TypeName)exp.visit(this, null);
		if(tn != TypeName.INTEGER) {
			throw new TypeCheckException(String.format("SleepStatement error at %s. (%s) Type of exp in sleep should be integer.", 
					sleepStatement.getFirstToken().getLinePos(), sleepStatement.getFirstToken().getText()));
		}
		return null;
	}

	@Override
	public Object visitWhileStatement(WhileStatement whileStatement, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Expression e = whileStatement.getE();
		TypeName tn = (TypeName)e.visit(this, null);
		if(tn != TypeName.BOOLEAN)
			throw new TypeCheckException(String.format("WhileStatement error at %s. (%s) Type of exp in while should be boolean.", 
					whileStatement.getFirstToken().getLinePos(), whileStatement.getFirstToken().getText()));
		Block b = whileStatement.getB();
		b.visit(this, null);
		return null;
	}

	@Override
	public Object visitDec(Dec declaration, Object arg) throws Exception {
		// TODO Auto-generated method stub
		boolean insertRes = this.symtab.insert(declaration.getIdent().getText(), declaration);
		if(!insertRes) {
			throw new TypeCheckException(String.format("Dec error at %s. (%s) Symbol table insert failure.", 
					declaration.getFirstToken().getLinePos(), declaration.getFirstToken().getText()));
		}
		return null;
	}

	@Override
	public Object visitProgram(Program program, Object arg) throws Exception {
		// TODO Auto-generated method stub
		String name = program.getName();
		ArrayList<ParamDec> params = program.getParams();
		for(ParamDec param: params) {
			param.visit(this, null);
		}
		Block b = program.getB();
		b.visit(this, null);
		if(this.outDetail) System.out.println(this.symtab);
		return null;
	}

	@Override
	public Object visitAssignmentStatement(AssignmentStatement assignStatement, Object arg) throws Exception {
		// TODO Auto-generated method stub
		IdentLValue identX = assignStatement.getVar();
		identX.visit(this, null);
		Expression exp = assignStatement.getE();
		TypeName expType = (TypeName)exp.visit(this, null);
		if(identX.getDec().getTypeName() != expType) {
			throw new TypeCheckException(String.format("AssignmentStatement error at %s. (%s) Types of lvalue and exp should be same.", 
					assignStatement.getFirstToken().getLinePos(), assignStatement.getFirstToken().getText()));
		}
		return null;
	}

	@Override
	public Object visitIdentLValue(IdentLValue identX, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Dec hDec = symtab.lookup(identX.getFirstToken().getText());
		if(hDec==null) 
			throw new TypeCheckException(String.format("IdentLValue error at %s. (%s) Variable is not declared before used.", 
					identX.getFirstToken().getLinePos(), identX.getFirstToken().getText()));
		else {
			identX.setDec(hDec);
			if(this.outDetail) {
				System.out.println("in visitIdentLValue");
				System.out.println(identX.getDec());
				System.out.println("--------------");
			}
			return null;
		}
	}

	@Override
	public Object visitParamDec(ParamDec paramDec, Object arg) throws Exception {
		// TODO Auto-generated method stub
		boolean insertRes = this.symtab.insert(paramDec.getIdent().getText(), paramDec);
		if(!insertRes) {
			throw new TypeCheckException(String.format("ParamDec error at %s. (%s) Symbol table insert failure.", 
					paramDec.getFirstToken().getLinePos(), paramDec.getFirstToken().getText()));
		}
		return null;
	}

	@Override
	public Object visitConstantExpression(ConstantExpression constantExpression, Object arg) {
		// TODO Auto-generated method stub
		constantExpression.setTypeName(TypeName.INTEGER);
		if(this.outDetail) {
			System.out.println("in visitConstantExpression");
			System.out.println(constantExpression);
			System.out.println("--------------");
		}
		return TypeName.INTEGER;
	}

	@Override
	public Object visitImageOpChain(ImageOpChain imageOpChain, Object arg) throws Exception {
		// TODO Auto-generated method stub
		Object retObj = null;
		Token firstToken = imageOpChain.getFirstToken();
		Tuple ht = imageOpChain.getArg();
		ht.visit(this, null);
		if(firstToken.isKind(Kind.OP_WIDTH, Kind.OP_HEIGHT)) {
			if(ht.getExprList().size()!=0) {
				throw new TypeCheckException(String.format("WIDTH or HEIGHT error at %s. (%s) Tuple length should be 0." , 
						imageOpChain.getFirstToken().getLinePos(), imageOpChain.getFirstToken().getText()));
			}
			imageOpChain.setTypeName(TypeName.INTEGER);
			retObj = TypeName.INTEGER;
		} else if(firstToken.isKind(Kind.KW_SCALE)) {
			if(ht.getExprList().size()!=1) {
				throw new TypeCheckException(String.format("SCALE error at %s. (%s) Tuple length should be 1." , 
						imageOpChain.getFirstToken().getLinePos(), imageOpChain.getFirstToken().getText()));
			}
			imageOpChain.setTypeName(TypeName.IMAGE);
			retObj = TypeName.IMAGE;
		} 
		if(this.outDetail) {
			System.out.println("in visitImageOpChain");
			System.out.println(imageOpChain);
			System.out.println("--------------");
		}
		if(retObj!=null) return retObj;
		throw new TypeCheckException(String.format("ImageOpChain error at %s. (%s)", 
				imageOpChain.getFirstToken().getLinePos(), imageOpChain.getFirstToken().getText()));
	}

	@Override
	public Object visitTuple(Tuple tuple, Object arg) throws Exception {
		// TODO Auto-generated method stub
		List<Expression> exps = tuple.getExprList();
		for(Expression exp: exps) {
			TypeName tn = (TypeName)exp.visit(this, null);
			if(tn != TypeName.INTEGER)
				throw new TypeCheckException(String.format("Tuple error at %s. (%s) Type of exp in tuple should be integer.", 
						exp.getFirstToken().getLinePos(), exp.getFirstToken().getText()));
		}
		return null;
	}


}
